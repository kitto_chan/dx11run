﻿//---------------------------------------------------------------------------
//!	@file	FontRenderer.h
//!	@brief	DirectXTKの文字描画
//! @note   TODO: 自分の文字描画シェーダを作る予定がある
//---------------------------------------------------------------------------
#pragma once
#include "DirectXTK/Inc/SpriteFont.h"
#include <map>
namespace render {
struct FontDesc
{
    FontDesc() = default;
    FontDesc(std::wstring msg, float2 pos,
             float4 color = { 1.0f, 1.0f, 1.0f, 1.0f },
             f32    size  = 1.0f)
    : _msg(msg)
    , _pos(pos)
    , _color(color)
    , _size(size)
    {
    }

    std::wstring _msg;                                      //!< 描画文字
    float2       _pos;                                      //!< 描画座標(スクリーン座標)
    float4       _color = float4(1.0f, 1.0f, 1.0f, 1.0f);   //!< 文字の色
    f32          _size  = 1.0f;                             //!< サイズ
};
//===========================================================================
//! フォントレンダリング管理
//===========================================================================
class FontRenderer : public Singleton<FontRenderer>
{
public:
    FontRenderer()  = default;
    ~FontRenderer() = default;

    // ムーブ禁止/コピー禁止
    FontRenderer(const FontRenderer&) = delete;
    FontRenderer(FontRenderer&&)      = delete;
    FontRenderer& operator=(const FontRenderer&) = delete;
    FontRenderer& operator=(FontRenderer&&) = delete;
    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
public:
    bool Init();       //!< 初期化
    void Update();     //!< 更新
    void Render();     //!< 描画
    void Finalize();   //!< 解放

	//! 描画文字を設定
    void SetFont(const std::string& key, const FontDesc& desc);

	//! 特定文字を削除
	//! @params [in] key 
	void RemoveFont(const std::string_view& key);

	//! フォントリストをクリアする
	void Clear();

private:
    uni_ptr<DirectX::SpriteBatch> _spriteBatch;   //!< バッチ
    uni_ptr<DirectX::SpriteFont>  _spriteFont;    //!< フォント

    std::map<std::string, FontDesc> _fontList;   //!< 文字描画リスト (key , desc)
};

render::FontRenderer* FontRendererIns();
}   // namespace render
