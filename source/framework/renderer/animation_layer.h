﻿//---------------------------------------------------------------------------
//!	@file	animation_layer.h
//!	@brief	3Dアニメーション(実装部)
//---------------------------------------------------------------------------
#pragma once

class AnimationLayerImpl;

//===========================================================================
//! アニメーションレイヤー
//! 単体のアニメーションを保持します
//===========================================================================
class AnimationLayer
{
public:
protected:
    //! デストラクタ
    virtual ~AnimationLayer() = default;
};

//! アニメーションレイヤーの読み込み
//! @param  [in]    path    ファイルパス
std::shared_ptr<AnimationLayer> createAnimationLayer(const char* path);