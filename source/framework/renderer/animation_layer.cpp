﻿//---------------------------------------------------------------------------
//!	@file	animation_layer.cpp
//!	@brief	3Dアニメーション
//---------------------------------------------------------------------------
#include "impl/model_impl.h"
#include "impl/animation_impl.h"
#include "impl/animation_layer_impl.h"

#include "OpenFBX/src/ofbx.h"
#include "import_fbx.h"

#include <iostream>
#include <fstream>
#include <filesystem>

//---------------------------------------------------------------------------
//! 時間指定でキーフレームを検索
//! 線形探索ではなくstd::upper_boundで二分探索高速化
//---------------------------------------------------------------------------
template<typename T>
[[nodiscard]] s32 AnimationLayerImpl::findKeyFrame(T container, f32 time) const
{
    auto it = std::upper_bound(std::begin(container), std::end(container), time);
    if(it == std::end(container)) {
        return static_cast<s32>(container.size() - 1);
    }
    return static_cast<s32>(std::distance(std::begin(container), it));
}

//---------------------------------------------------------------------------
//! 補間されたポーズを計算
//---------------------------------------------------------------------------
template<bool USE_WEIGHT>
void AnimationLayerImpl::getRelativePose(f32 time, Pose& pose, const ModelImpl& model, f32 weight) const
{
    auto& positions = pose.positions_;
    auto& rotations = pose.rotations_;

    // アニメーション時間 (先頭0.0f～終端1.0f)
    const f32 animationT = std::min(time / animationLength(), 1.0f);

    //----------------------------------------------------------
    // Translationの補間サンプリング
    //----------------------------------------------------------
    for(auto& curve : curvesT_) {
        // ボーン番号を取得(リグ構成が異なるアニメーションのために都度検索)
        s32 boneIndex = model.boneIndexFromHash(curve.name_);
        if(boneIndex < 0)
            continue;

        //--------------------------------------------------
        // キーフレームから２つのキーを選択して補間
        //--------------------------------------------------
        float3 p;   // 補間済の座標
        if(!curve.times_.empty()) {
            s32 index = findKeyFrame(curve.times_, animationT);   // キーフレームを検索

            // 補間
            f32 t = 0.0f;
            if(curve.times_[index] != curve.times_[index - 1]) {
                t = static_cast<f32>(animationT - curve.times_[index - 1]) / (curve.times_[index] - curve.times_[index - 1]);
            }

            p = lerp(curve.positions_[index - 1], curve.positions_[index], t);
        }

        if constexpr(USE_WEIGHT) {
            positions[boneIndex] = lerp(positions[boneIndex], p, weight);
        }
        else {
            positions[boneIndex] = p;
        }
    }

    //----------------------------------------------------------
    // Rotationの補間サンプリング
    //----------------------------------------------------------
    for(auto& curve : curvesR_) {
        // ボーン番号を取得(リグ構成が異なるアニメーションのために都度検索)
        s32 boneIndex = model.boneIndexFromHash(curve.name_);
        if(boneIndex < 0)
            continue;

        //--------------------------------------------------
        // キーフレームから２つのキーを選択して補間
        //--------------------------------------------------
        quaternion r;   // 補間済の回転
        if(!curve.times_.empty()) {
            s32 index = findKeyFrame(curve.times_, animationT);   // キーフレームを検索

            // 補間
            f32 t = 0.0f;
            if(curve.times_[index] != curve.times_[index - 1]) {
                t = static_cast<f32>(animationT - curve.times_[index - 1]) / (curve.times_[index] - curve.times_[index - 1]);
            }

            r = nlerp(curve.rotations_[index - 1], curve.rotations_[index], t);
        }

        if constexpr(USE_WEIGHT) {
            rotations[boneIndex] = nlerp(rotations[boneIndex], r, weight);
        }
        else {
            rotations[boneIndex] = r;
        }
    }
}

//---------------------------------------------------------------------------
//! ポーズを更新
//---------------------------------------------------------------------------
void AnimationLayerImpl::updatePose(f32 time, Pose& pose, const ModelImpl* model, f32 weight) const
{
    if(weight < 0.9999f) {
        getRelativePose<true>(time, pose, *model, weight);
    }
    else {
        getRelativePose<false>(time, pose, *model, weight);
    }
}

//---------------------------------------------------------------------------
//! アニメーションレイヤの読み込み
//---------------------------------------------------------------------------
std::shared_ptr<AnimationLayer> createAnimationLayer(const char* path)
{
    std::unique_ptr<importer::ImportFbx> importFbx = importer::createImportFbx(path, 1.0f, importer::ImportFbx::FLAG_IMPORT_ANIMATION);

    if(!importFbx) {
        return nullptr;
    }

    // アニメーションにコンバート
    auto animationLayer = importFbx->convertToAnimation(0);
    return animationLayer;
}
