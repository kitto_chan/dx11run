﻿//---------------------------------------------------------------------------
//!	@file	animation.h
//!	@brief	3Dアニメーション
//---------------------------------------------------------------------------
#pragma once

class AnimationLayer;

//===========================================================================
//! 3Dアニメーション
//===========================================================================
class Animation
{
public:
    // アニメーション設定
    struct Desc
    {
        const char*      name_;   //!< アニメーション名(任意)
        std::string_view path_;   //!< アニメーションファイルパス
    };

    //! アニメーション再生タイプ
    enum class PlayType
    {
        Once,   //!< 一度だけ再生
        Loop,   //!< ループ再生
    };

    //! デストラクタ
    virtual ~Animation() = default;

    //! 更新
    //! @param  [in]    t       進める時間(単位:秒)
    virtual void update(f32 t) = 0;

    //! アニメーション再生リクエスト
    //! @param  [in]    name    名前
    //! @param  [in]    playType    アニメーション再生タイプ
    virtual bool play(const char* name, Animation::PlayType playType) = 0;

    //! アニメーションレイヤーを追加
    //! @param  [in]    name    アニメーション名(任意)
    //! @param  [in]    layer   アニメーションレイヤー
    virtual void appendLayer(const char* name, std::shared_ptr<AnimationLayer> layer) = 0;

    //! アニメーションレイヤーの数を取得
    virtual size_t animationLayerCount() const = 0;

    //! アニメーションレイヤーを取得
    //! @param  [in]    name    アニメーション名
    virtual const AnimationLayer* animationLayer(const char* name) const = 0;

protected:
    Animation() = default;
};

//! アニメーション作成
//! @param  [in]    desc    アニメーションファイル一覧
//! @param  [in]    count   アニメーション個数
std::shared_ptr<Animation> createAnimation(const Animation::Desc* desc, size_t count);
