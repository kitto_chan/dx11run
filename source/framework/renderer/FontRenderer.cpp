﻿//---------------------------------------------------------------------------
//!	@file	FontRenderer.h
//!	@brief	DirectXTKの文字描画
//! @note   TODO: 自分の文字描画シェーダを作る予定がある
//---------------------------------------------------------------------------
#include "FontRenderer.h"
#include <sstream>
#include <iostream>
namespace render {
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool FontRenderer::Init()
{
    _spriteBatch = std::make_unique<DirectX::SpriteBatch>(dx11::context());
    _spriteFont  = std::make_unique<DirectX::SpriteFont>(dx11::d3dDevice(), L"font/myfileb.spritefont");

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------

void FontRenderer::Update()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void FontRenderer::Render()
{
    _spriteBatch->Begin();

    for(auto iter = _fontList.begin(); iter != _fontList.end(); ++iter) {
        FontDesc fontDesc = iter->second;

        const wchar_t* msg = fontDesc._msg.c_str();

        // 中心点を計算
        DirectX::XMVECTOR centerPos = _spriteFont->MeasureString(msg);
        centerPos.m128_f32[0] /= 2.0f;
        centerPos.m128_f32[1] /= 2.0f;

        // 描画座標
        DirectX::XMVECTOR renderPos = { fontDesc._pos.f32[0], fontDesc._pos.f32[1] };

        _spriteFont->DrawString(_spriteBatch.get(),
                                msg,
                                renderPos,
                                math::castXMVector(fontDesc._color),
                                0.0f,
                                centerPos,
                                fontDesc._size);
    }

    _spriteBatch->End();
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------

void FontRenderer::Finalize()
{
}
//---------------------------------------------------------------------------
//! 描画文字を設定
//---------------------------------------------------------------------------
void FontRenderer::SetFont(const std::string& key,const FontDesc& desc)
{
    _fontList[key] = desc;
}
//---------------------------------------------------------------------------
//! 特定文字を削除
//! @params [in] key
//---------------------------------------------------------------------------
void FontRenderer::RemoveFont(const std::string_view& key)
{
    _fontList.erase(key.data());
}
//---------------------------------------------------------------------------
//! フォントリストをクリアする
//---------------------------------------------------------------------------
void FontRenderer::Clear()
{
    _fontList.clear();
}
//============================================================================
// 実体を取得
//============================================================================
render::FontRenderer* FontRendererIns()
{
    return render::FontRenderer::Instance();
}

}   // namespace render
