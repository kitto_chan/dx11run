﻿//---------------------------------------------------------------------------
//!	@file	KeyBoardManager.h
//!	@brief	キーボードマネジャー
//---------------------------------------------------------------------------
#pragma once

namespace input {
using dx_kb = DirectX::Keyboard;
class KeyboardManager : public Singleton<KeyboardManager>
{
public:
    KeyboardManager();
    ~KeyboardManager();

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------
    //bool Init();     //! 初期化
    void               Update();   //! 更新
    bool               IsKeyDown(DirectX::Keyboard::Keys key);
    bool               IsKeyPress(DirectX::Keyboard::Keys key);
    DirectX::Keyboard* GetKeyboard();

private:
    DirectX::Keyboard::State                _keyState;
    DirectX::Keyboard::State                _lastKeyState;
    DirectX::Keyboard::KeyboardStateTracker _keyboardTracker;

    uni_ptr<DirectX::Keyboard> _pKeyboard;
};
//! ゲーム管理クラスを取得
input::KeyboardManager* KeyboardMgr();

}   // namespace input
