﻿//---------------------------------------------------------------------------
//!	@file	MouseManager.cpp
//!	@brief	マウスマネジャー
//! @ref https://github.com/microsoft/DirectXTK/wiki/Mouse
//---------------------------------------------------------------------------
#include "MouseManager.h"

namespace input {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
MouseManager::MouseManager()
{
    _pMouse = std::make_unique<DirectX::Mouse>();
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
MouseManager::~MouseManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool MouseManager::Init(HWND hwnd)
{
    _pMouse->SetWindow(hwnd);
    //SetRelativeMode();

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void MouseManager::Update()
{
    _mouseState     = _pMouse->GetState();
    _lastMouseState = _mouseTracker.GetLastState();
    _mouseTracker.Update(_mouseState);

    if(input::KeyboardMgr()->IsKeyPress(dx_kb::LeftControl) &&
       IsRelativeMode()) {
        SetAbsoluteMode();
    }
    if(input::KeyboardMgr()->IsKeyPress(dx_kb::LeftControl) &&
       IsAbsoluteMode()) {
        SetRelativeMode();
    }
}
void MouseManager::SetMode(DirectX::Mouse::Mode mode)
{
    _pMouse->SetMode(mode);
}
void MouseManager::SetRelativeMode()
{
    SetMode(DirectX::Mouse::MODE_RELATIVE);
}
void MouseManager::SetAbsoluteMode()
{
    SetMode(DirectX::Mouse::MODE_ABSOLUTE);
}
bool MouseManager::IsRelativeMode()
{
    return _mouseState.positionMode == DirectX::Mouse::MODE_RELATIVE;
}
bool MouseManager::IsAbsoluteMode()
{
    return _mouseState.positionMode == DirectX::Mouse::MODE_ABSOLUTE;
}
bool MouseManager::IsLeftButton()
{
    return _mouseState.leftButton;
}
void MouseManager::ResetScrollWheelValue()
{
    _pMouse->ResetScrollWheelValue();
}
s32 MouseManager::GetPosX()
{
    return _mouseState.x;
}
s32 MouseManager::GetPosY()
{
    return _mouseState.y;
}
s32 MouseManager::GetScrollWheelValue()
{
    return _mouseState.scrollWheelValue;
}
DirectX::Mouse* MouseManager::GetMouse()
{
    return _pMouse.get();
}
DirectX::Mouse::State MouseManager::GetMouseState()
{
    return _mouseState;
}
input::MouseManager* MouseMgr()
{
    return MouseManager::Instance();
}
}   // namespace input
