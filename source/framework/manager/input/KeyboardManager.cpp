﻿//---------------------------------------------------------------------------
//!	@file	KeyBoardManager.cpp
//!	@brief	キーボードマネジャー
//---------------------------------------------------------------------------
#include "KeyboardManager.h"

namespace input {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
KeyboardManager::KeyboardManager()
{
    _pKeyboard = std::make_unique<DirectX::Keyboard>();
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
KeyboardManager::~KeyboardManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
//bool KeyboardManager::Init()
//{
//    return false;
//}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void KeyboardManager::Update()
{
    _keyState = _pKeyboard->GetState();
    _lastKeyState = _keyboardTracker.GetLastState();
    _keyboardTracker.Update(_keyState);
}

bool KeyboardManager::IsKeyDown(DirectX::Keyboard::Keys key)
{
    return _keyState.IsKeyDown(key);
}

bool KeyboardManager::IsKeyPress(DirectX::Keyboard::Keys key)
{
    return _keyboardTracker.IsKeyPressed(key);
}

DirectX::Keyboard* KeyboardManager::GetKeyboard()
{
    return _pKeyboard.get();
}
input::KeyboardManager* KeyboardMgr()
{
    return KeyboardManager::Instance();
}

}   // namespace input