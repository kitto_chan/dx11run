﻿//---------------------------------------------------------------------------
//!	@file	animation_impl.h
//!	@brief	3Dアニメーション(実装部)
//---------------------------------------------------------------------------
#pragma once

//===========================================================================
//! 3Dアニメーション(実装部)
//===========================================================================
class AnimationImpl final : public Animation
{
public:
    //! デフォルトコンストラクタ
    AnimationImpl() = default;

    //! デストラクタ
    virtual ~AnimationImpl() = default;

    //! アニメーションレイヤー個数を取得
    size_t layerCount() const { return animationLayers_.size(); }

    //! 更新
    //! @param  [in]    t       進める時間(単位:秒)
    virtual void update(f32 t) override;

    //! アニメーション再生リクエスト
    //! @param  [in]    name    名前
    //! @param  [in]    playType    アニメーション再生タイプ
    virtual bool play(const char* name, Animation::PlayType playType) override;

    //! アニメーションレイヤーを追加
    //! @param  [in]    name    アニメーション名(任意)
    //! @param  [in]    layer   アニメーションレイヤー
    virtual void appendLayer(const char* name, std::shared_ptr<AnimationLayer> layer) override;

    //! アニメーションレイヤーの数を取得
    virtual size_t animationLayerCount() const override;

    //! アニメーションレイヤーを取得
    //! @param  [in]    name    アニメーション名
    virtual const AnimationLayer* animationLayer(const char* name) const override;

    //! ポーズ情報の作成
    void buildPose(Pose& pose, const ModelImpl* model);

private:
    f32                 time_        = 0.0f;      //!< 現在の再生時間
    AnimationLayerImpl* activeLayer_ = nullptr;   //!< 現在再生中のアニメーションレイヤー
    Animation::PlayType playType_;                //!< アニメーション再生タイプ

    //! アニメーションレイヤー
    std::unordered_map<std::string, std::shared_ptr<AnimationLayerImpl>> animationLayers_;
};
