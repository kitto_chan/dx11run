﻿//---------------------------------------------------------------------------
//!	@file	animation_layer_impl.h
//!	@brief	3Dアニメーションレイヤー(実装部)
//---------------------------------------------------------------------------
#pragma once

#include "animation_layer.h"

struct Pose;
class ModelImpl;

//---------------------------------------------------------------------------
//! NLERP
//! SLERPよりも低精度/高速。SLERPとは異なり、q1とq2の値が一致しても補間できる
//---------------------------------------------------------------------------
inline quaternion nlerp(const quaternion& q1, const quaternion& q2, f32 t)
{
    if(dot(q1, q2).x < 0.0f)
        t = -t;

    return normalize(lerp(q1, q2, t));
}

//===========================================================================
//! アニメーションレイヤー(実装部)
//! 単体のアニメーションを保持します
//===========================================================================
class AnimationLayerImpl : public AnimationLayer
{
public:
    struct CurveT;
    struct CurveR;

    //---------------------------------------------------------------------------
    //! 時間指定でキーフレームを検索
    //! 線形探索ではなくstd::upper_boundで二分探索高速化
    //---------------------------------------------------------------------------
    template<typename T>
    [[nodiscard]] s32 findKeyFrame(T container, f32 time) const;

    //---------------------------------------------------------------------------
    //! 補間されたポーズを計算
    //---------------------------------------------------------------------------
    template<bool USE_WEIGHT>
    void getRelativePose(f32 time, Pose& pose, const ModelImpl& model, f32 weight) const;

    //! ポーズを更新
    void updatePose(f32 t, Pose& pose, const ModelImpl* model, f32 weight = 1.0f) const;

    //! アニメーションの長さを取得(単位:秒)
    f32 animationLength() const { return animationLength_; }

public:
    AnimationLayerImpl() = default;

public:
    f32                                     animationLength_;   //!< アニメーションの長さ(単位:秒)
    std::vector<AnimationLayerImpl::CurveT> curvesT_;           //!< アニメーションカーブ(平行移動)
    std::vector<AnimationLayerImpl::CurveR> curvesR_;           //!< アニメーションカーブ(回転)
};

//--------------------------------------------------------------
//! アニメーションカーブ(平行移動)
//--------------------------------------------------------------
struct AnimationLayerImpl::CurveT
{
    u32                 name_ = 0;    //!< 名前ハッシュ(boneHashMap_に対応)
    std::vector<f32>    times_;       //!< 時間
    std::vector<float3> positions_;   //!< 位置座標
};

//--------------------------------------------------------------

//! アニメーションカーブ(回転)
//--------------------------------------------------------------
struct AnimationLayerImpl::CurveR
{
    u32                     name_ = 0;    //!< 名前ハッシュ(boneHashMap_に対応)
    std::vector<f32>        times_;       //!< 時間
    std::vector<quaternion> rotations_;   //!< 回転
};
