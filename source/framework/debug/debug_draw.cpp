﻿//---------------------------------------------------------------------------
//!	@file	debug_draw.cpp
//!	@brief	デバッグ描画
//---------------------------------------------------------------------------
#include "debug_draw.h"
#include "impl/debug_draw_impl.h"

//===========================================================================
//! デバッグ描画(実装部)
//! 描画発行をまとめて一括発行します。
//===========================================================================
class DebugDrawImpl final
{
public:
    //! プリミティブの種類
    enum class Type
    {
        Line,       //!< ライン
        Triangle,   //!< 三角形
    };

    //----------------------------------------------------------
    // 頂点フォーマット
    //----------------------------------------------------------
    struct Vertex
    {
        DirectX::XMFLOAT3 position_;   //!< 座標
        Color             color_;      //!< カラー
    };

    //----------------------------------------------------------
    //! プリミティブ基底
    //----------------------------------------------------------
    struct Command
    {
        union
        {
            struct
            {
                Type type_ : 4;         //!< プリミティブの種類
                u32  depthUse_ : 1;     //!< デプス使用
                u32  depthWrite_ : 1;   //!< デプス書き込み
                u32  reserved_ : 26;    //!< 未使用
            };
            u32 state_ = 0;   //!< 32bit一括アクセス
        };
        u32     count_ = 0;   //!< 頂点数
        uintptr offset_;      //!< 先頭からのオフセット位置
    };

public:
    //! シングルトンオブジェクトを取得
    static DebugDrawImpl& instance()
    {
        static DebugDrawImpl instance;
        return instance;
    }

    //! 初期化
    bool initialize();

    //! 描画情報をフラッシュ
    void flush();

    //! 解放
    void finalize();

    //! 描画コマンド追加
    //! @param  [in]    type        プリミティブの種類
    //! @param  [in]    vertexCount 頂点数
    //! @param  [in]    flags       描画フラグ(debug::DRAW_FLAGの組み合わせ)
    //! @return 確保されたメモリの先頭
    Vertex* draw(DebugDrawImpl::Type type, size_t vertexCount, u32 flags);

private:
    //----------------------------------------------------------
    //! @defgroup   シングルトンオブジェクト
    //----------------------------------------------------------
    //@{

    //! コンストラクタ
    DebugDrawImpl() = default;

    //! デストラクタ
    virtual ~DebugDrawImpl() = default;

    //@}
private:
    // コピー禁止/move禁止
    DebugDrawImpl(const DebugDrawImpl&) = delete;
    DebugDrawImpl(DebugDrawImpl&&)      = delete;
    DebugDrawImpl& operator=(const DebugDrawImpl&) = delete;
    DebugDrawImpl& operator=(DebugDrawImpl&&) = delete;

private:
    //! バッファをメモリマップ開始
    void map();

    //! バッファをメモリマップ終了
    void unmap();

private:
    Vertex* vertices_   = nullptr;   //!< マップされたメモリの先頭アドレス
    uintptr usedSizeVb_ = 0;         //!< 使用中のバッファサイズ

    std::shared_ptr<dx11::Buffer>      bufferVb_;      //!< 頂点バッファ
    std::shared_ptr<dx11::InputLayout> inputLayout_;   //!< 入力レイアウト
    std::shared_ptr<dx11::Shader>      shaderVs_;      //!< VS 3D頂点シェーダー
    std::shared_ptr<dx11::Shader>      shaderPs_;      //!< PS ピクセルシェーダー

    std::vector<Command> commands_;   //!< 描画コマンド
};

//--------------------------------------------------------------------------
//! 初期化
//--------------------------------------------------------------------------
bool DebugDrawImpl::initialize()
{
    constexpr u32 size = 16 * 1024 * 1024;   // 16MB

    //----------------------------------------------------------
    // 動的バッファを作成(D3D11_USAGE_DYNAMIC)
    //----------------------------------------------------------
    bufferVb_ = dx11::createBuffer({ size, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC });   // 頂点バッファ
    if(!bufferVb_) {
        return false;
    }

    //-------------------------------------------------------------
    // シェーダーをコンパイル
    //-------------------------------------------------------------
    shaderVs_ = dx11::createShader("framework/vs_debug_3d.fx", "main", "vs_5_0");   // 頂点シェーダー
    shaderPs_ = dx11::createShader("framework/ps_debug.fx", "main", "ps_5_0");      // ピクセルシェーダー
    if(!shaderVs_ || !shaderPs_) {
        return false;
    }
    //----------------------------------------------------------
    // 入力レイアウトの作成
    //----------------------------------------------------------
    // clang-format off
    static constexpr D3D11_INPUT_ELEMENT_DESC layout[]{
        // セマンテック名(任意)                ストリームスロット番号(0-15)
        // ↓  セマンテック番号(0～7)                    ↓    構造体の先頭からのオフセットアドレス(先頭からnバイト目)
        // ↓        , ↓,         データ形式          , ↓,    ↓                       , 頂点読み込みの更新周期       , 更新間隔
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Vertex, position_), D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "COLOR"   , 0, DXGI_FORMAT_R8G8B8A8_UNORM , 0, offsetof(Vertex, color_   ), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };
    // clang-format on

    inputLayout_ = dx11::createInputLayout(layout, std::size(layout));
    if(!inputLayout_) {
        return false;
    }

    //----------------------------------------------------------
    // 頂点バッファをマップ
    //----------------------------------------------------------
    map();

    return true;
}

//--------------------------------------------------------------------------
//! 描画情報をフラッシュ
//--------------------------------------------------------------------------
void DebugDrawImpl::flush()
{
    //----------------------------------------------------------
    // 頂点バッファをマップ解除
    //----------------------------------------------------------
    unmap();

    //----------------------------------------------------------
    // 描画発行
    //----------------------------------------------------------
    dx11::setInputLayout(inputLayout_);   // 入力レイアウト

    // シェーダー
    dx11::vs::setShader(shaderVs_);   // VS 頂点シェーダー (3D)
    dx11::ps::setShader(shaderPs_);   // PS ピクセルシェーダー (テクスチャあり)

    // 半透明ON
    dx11::setBlendState(dx11::commonStates().NonPremultiplied());

    for(auto& c : commands_) {
        // デプスステンシルステート
        if(c.depthUse_) {
            if(c.depthWrite_) {
                // デプスON / デプス書き込みON
                dx11::setDepthStencilState(dx11::commonStates().DepthDefault());
            }
            else {
                // デプスON / デプス書き込みOFF
                dx11::setDepthStencilState(dx11::commonStates().DepthRead());
            }
        }
        else {
            // デプスOFF
            dx11::setDepthStencilState(dx11::commonStates().DepthNone());
        }

        // 頂点バッファ
        dx11::setVertexBuffer(0, bufferVb_, 0, static_cast<u32>(c.offset_));

        // 描画
        if(c.type_ == Type::Line) {
            dx11::draw(dx11::Primitive::LineList, c.count_);
        }
        else {
            dx11::draw(dx11::Primitive::TriangleList, c.count_);
        }
    }

    //----------------------------------------------------------
    // 頂点バッファをマップ
    //----------------------------------------------------------
    map();

    // 元に戻す
    dx11::setDepthStencilState(dx11::commonStates().DepthDefault());
    dx11::setBlendState(dx11::commonStates().Opaque());
}

//--------------------------------------------------------------------------
//! 解放
//--------------------------------------------------------------------------
void DebugDrawImpl::finalize()
{
    //----------------------------------------------------------
    // 頂点バッファをマップ解除
    //----------------------------------------------------------
    unmap();

    //----------------------------------------------------------
    // 明示的に解放
    //----------------------------------------------------------
    bufferVb_.reset();      // 頂点バッファ
    inputLayout_.reset();   // 入力レイアウト
    shaderVs_.reset();      // VS 3D頂点シェーダー
    shaderPs_.reset();      // PS ピクセルシェーダー
}

//--------------------------------------------------------------------------
//! ライン描画コマンド追加
//--------------------------------------------------------------------------
DebugDrawImpl::Vertex* DebugDrawImpl::draw(DebugDrawImpl::Type type, size_t vertexCount, u32 flags)
{
    size_t allocSize = sizeof(Vertex) * vertexCount;   // 確保サイズ

    // バッファサイズのオーバーフローチェック
    if(bufferVb_->desc().size_ < usedSizeVb_ + allocSize) {
        ASSERT_MESSAGE(false, "デバッグ描画のバッファサイズが足りません。");
        return nullptr;
    }

    //----------------------------------------------------------
    // コマンドを登録
    //----------------------------------------------------------
    Command commandFlags{};   // フラグ構築

    commandFlags.type_       = type;
    commandFlags.depthUse_   = (flags & debug::DRAW_DEPTH_USE) ? 1 : 0;
    commandFlags.depthWrite_ = (flags & debug::DRAW_DEPTH_WRITE) ? 1 : 0;

    // 既存のコマンドを属性が同じだった場合
    if(commands_.size() > 0 && commands_.back().state_ == commandFlags.state_) {
        // 既存のコマンドの個数だけを増加させる
        commands_.back().count_ += static_cast<u32>(vertexCount);
    }
    else {
        // コマンドを新規追加
        Command command{};
        command.state_  = commandFlags.state_;
        command.count_  = static_cast<u32>(vertexCount);
        command.offset_ = usedSizeVb_;
        commands_.emplace_back(std::move(command));
    }

    //----------------------------------------------------------
    // 先頭メモリアドレスとサイズ計算
    //----------------------------------------------------------
    auto* p = reinterpret_cast<DebugDrawImpl::Vertex*>(reinterpret_cast<uintptr>(vertices_) + usedSizeVb_);
    usedSizeVb_ += allocSize;

    return p;
}

//--------------------------------------------------------------------------
//! バッファをメモリマップ開始
//--------------------------------------------------------------------------
void DebugDrawImpl::map()
{
    // バッファをDISCARDで先頭から利用
    vertices_ = static_cast<Vertex*>(bufferVb_->map(D3D11_MAP_WRITE_DISCARD));
    ASSERT_MESSAGE(vertices_, "map error.");

    usedSizeVb_ = 0;   // 使用中のバッファサイズ
    commands_.clear();
}

//--------------------------------------------------------------------------
//! バッファをメモリマップ終了
//--------------------------------------------------------------------------
void DebugDrawImpl::unmap()
{
    if(vertices_) {
        bufferVb_->unmap();
        vertices_ = nullptr;
    }
}

//===========================================================================
// システム用アクセス関数
//===========================================================================
namespace debug {

//--------------------------------------------------------------------------
//! デバッグ描画の初期化
//--------------------------------------------------------------------------
bool initializeDebugDraw()
{
    return DebugDrawImpl::instance().initialize();
}

//--------------------------------------------------------------------------
//! デバッグ描画の一括発行
//--------------------------------------------------------------------------
void flushDebugDraw()
{
    return DebugDrawImpl::instance().flush();
}

//--------------------------------------------------------------------------
//! デバッグ描画の解放
//--------------------------------------------------------------------------
void finalizeDebugDraw()
{
    return DebugDrawImpl::instance().finalize();
}

}   // namespace debug

//===========================================================================
//  デバッグ描画関数群
//===========================================================================
namespace debug {

//--------------------------------------------------------------------------
//! ライン描画(Flat)
//--------------------------------------------------------------------------
void drawLineF(const float3& p0, const float3& p1, const Color& c, u32 flags)
{
    debug::drawLineG(p0, p1, c, c, flags);
}

//---------------------------------------------------------------------------
//! 三角形描画(Flat)
//---------------------------------------------------------------------------
void drawTriangleF(const float3& p0, const float3& p1, const float3& p2, const Color& c, u32 flags)
{
    debug::drawTriangleG(p0, p1, p2, c, c, c, flags);
}

//--------------------------------------------------------------------------
//! ライン描画(Gouraud)
//--------------------------------------------------------------------------
void drawLineG(const float3& p0, const float3& p1, const Color& c0, const Color& c1, u32 flags)
{
    // 描画登録とメモリ確保
    auto p = DebugDrawImpl::instance().draw(DebugDrawImpl::Type::Line, 2, flags);
    if(!p)
        return;

    // 頂点バッファに書き込み
    p[0].position_ = DirectX::XMFLOAT3(p0.x, p0.y, p0.z);
    p[0].color_    = c0;
    p[1].position_ = DirectX::XMFLOAT3(p1.x, p1.y, p1.z);
    p[1].color_    = c1;
}

//---------------------------------------------------------------------------
//! 三角形描画(Gouraud)
//---------------------------------------------------------------------------
void drawTriangleG(const float3& p0, const float3& p1, const float3& p2, const Color& c0, const Color& c1, const Color& c2, u32 flags)
{
    // 描画登録とメモリ確保
    auto p = DebugDrawImpl::instance().draw(DebugDrawImpl::Type::Triangle, 3, flags);
    if(!p)
        return;

    // 頂点バッファに書き込み
    p[0].position_ = DirectX::XMFLOAT3(p0.x, p0.y, p0.z);
    p[0].color_    = c0;
    p[1].position_ = DirectX::XMFLOAT3(p1.x, p1.y, p1.z);
    p[1].color_    = c1;
    p[2].position_ = DirectX::XMFLOAT3(p2.x, p2.y, p2.z);
    p[2].color_    = c2;
}

//---------------------------------------------------------------------------
// 行列の描画
//---------------------------------------------------------------------------
void drawMatrix(const matrix& m, f32 scale, u32 flags)
{
    float3 axisX    = m._11_12_13;
    float3 axisY    = m._21_22_23;
    float3 axisZ    = m._31_32_33;
    float3 position = m._41_42_43;

    debug::drawLineF(position, position + axisX * scale, Color(255, 0, 0), flags);
    debug::drawLineF(position, position + axisY * scale, Color(0, 255, 0), flags);
    debug::drawLineF(position, position + axisZ * scale, Color(0, 0, 255), flags);
}

//---------------------------------------------------------------------------
// ボーンの描画
//---------------------------------------------------------------------------
void drawBone(const float3 p0, const float3 p1, const Color& c, u32 flags)
{
    float3 to    = p1 - p0;
    f32    scale = length(to) * 0.1f;
    float3 right = normalize(cross(to, math::AXIS_Y.xyz)) * scale;
    float3 top   = normalize(cross(to, right)) * scale;

    float3 p = p0 + to * 0.2f;   // 始点と終点の位置の比率を2:8に分割した中間点

    // 中心軸
    debug::drawLineF(p0, p1, c, flags);

    float3 v[4]{
        // 四隅座標
        float3(p - right),
        float3(p - top),
        float3(p + right),
        float3(p + top),
    };

    // 十字の軸
    debug::drawLineF(v[0], v[1], c, flags);
    debug::drawLineF(v[2], v[3], c, flags);

    // 外周部分
    debug::drawLineF(v[0], v[1], c, flags);
    debug::drawLineF(v[1], v[2], c, flags);
    debug::drawLineF(v[2], v[3], c, flags);
    debug::drawLineF(v[3], v[0], c, flags);

    // 縦方向
    debug::drawLineF(p0, v[0], c, flags);
    debug::drawLineF(p0, v[1], c, flags);
    debug::drawLineF(p0, v[2], c, flags);
    debug::drawLineF(p0, v[3], c, flags);
    debug::drawLineF(p1, v[0], c, flags);
    debug::drawLineF(p1, v[1], c, flags);
    debug::drawLineF(p1, v[2], c, flags);
    debug::drawLineF(p1, v[3], c, flags);
}

//---------------------------------------------------------------------------
//! 矢印の描画
//---------------------------------------------------------------------------
void drawArrow(const float3& position, const float3& dir, const Color& color, u32 flags)
{
    float3 dirX = cross(dir, float3(0.0f, 1.0f, 0.0f));
    if(dot(dirX, dirX).x < 0.00001f) {
        dirX = cross(dir, float3(0.0f, 0.0f, 1.0f));
    }

    float3 dirY = cross(dir, dirX);

    f32 lengthSq = dot(dir, dir);
    if(lengthSq < 0.001f) {
        return;
    }

    f32 L = length(dir);
    dirX  = normalize(dirX) * L * 0.1f;
    dirY  = normalize(dirY) * L * 0.1f;

    float3 base = position + dir * 0.6f;   // dirのベクトルを4:6に分割した位置
    float3 s    = position;
    float3 e    = position + dir;
    // 中心軸
    debug::drawLineF(s, e, color, flags);

    // 傘部分
    debug::drawLineF(e, base + dirX, color, flags);
    debug::drawLineF(e, base - dirX, color, flags);
    debug::drawLineF(e, base + dirY, color, flags);
    debug::drawLineF(e, base - dirY, color, flags);

    // 傘部分の土台
    debug::drawLineF(base + dirX, base + dirY, color, flags);
    debug::drawLineF(base + dirY, base - dirX, color, flags);
    debug::drawLineF(base - dirX, base - dirY, color, flags);
    debug::drawLineF(base - dirY, base + dirX, color, flags);

    // 傘部分の軸
    debug::drawLineF(base - dirX, base + dirX, color, flags);
    debug::drawLineF(base - dirY, base + dirY, color, flags);
}

//---------------------------------------------------------------------------
//! 法線の描画
//---------------------------------------------------------------------------
void drawNormal(const float3& position, const float3& normal, const Color& color, u32 flags)
{
    float3 dirX = cross(normal, float3(0.0f, 1.0f, 0.0f));
    if(dot(dirX, dirX).x < 0.00001f) {
        dirX = cross(normal, float3(0.0f, 0.0f, 1.0f));
    }

    float3 dirY = cross(normal, dirX);

    dirX = normalize(dirX) * 0.25f;
    dirY = normalize(dirY) * 0.25f;

    // 土台の円
    constexpr u32 divCount = 8;   // 分割数

    for(u32 i = 0; i < divCount; ++i) {
        f32    angle0 = (i + 0) * (1.0f / static_cast<f32>(divCount)) * (math::PI * 2.0f);
        f32    angle1 = (i + 1) * (1.0f / static_cast<f32>(divCount)) * (math::PI * 2.0f);
        float3 p0     = position + dirX * cosf(angle0) + dirY * sinf(angle0);
        float3 p1     = position + dirX * cosf(angle1) + dirY * sinf(angle1);

        debug::drawLineF(p0, p1, color);
    }

    // 矢印
    debug::drawArrow(position, normal * 0.25f, color, flags);
}

//---------------------------------------------------------------------------
//! 球の描画
//---------------------------------------------------------------------------
void drawSphere(const float3& position, f32 radius, const Color& color, [[maybe_unused]] u32 flags)
{
    // Bulletのデバッグ描画を利用
    auto*         world     = physics::PhysicsEngine::instance()->dynamicsWorld();
    btIDebugDraw* debugDraw = world->getDebugDrawer();

    f32 r = static_cast<f32>(color.r_) * (1.0f / 255.0f);
    f32 g = static_cast<f32>(color.g_) * (1.0f / 255.0f);
    f32 b = static_cast<f32>(color.b_) * (1.0f / 255.0f);

    debugDraw->drawSphere(btVector3(position.x, position.y, position.z), radius, btVector3(r, g, b));
}

}   // namespace debug
