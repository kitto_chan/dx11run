﻿//---------------------------------------------------------------------------
//!	@file	debug_draw.h
//!	@brief	デバッグ描画
//---------------------------------------------------------------------------
#pragma once

namespace debug {

//! 描画フラグ
enum DRAW_FLAG
{
    DRAW_DEPTH_USE   = (1u << 0),
    DRAW_DEPTH_WRITE = (1u << 1),
};

// デフォルトの設定(デプスON / デプス書き込みは行わない)
constexpr u32 DefaultDrawFlags = DRAW_DEPTH_USE;

//! ライン描画(Flat)
void drawLineF(const float3& p0, const float3& p1, const Color& c, u32 flags = debug::DefaultDrawFlags);

//! 三角形描画(Flat)
void drawTriangleF(const float3& p0, const float3& p1, const float3& p2, const Color& c, u32 flags = debug::DefaultDrawFlags);

//! ライン描画(Gouraud)
void drawLineG(const float3& p0, const float3& p1, const Color& c0, const Color& c1, u32 flags = debug::DefaultDrawFlags);

//! 三角形描画(Gouraud)
void drawTriangleG(const float3& p0, const float3& p1, const float3& p2, const Color& c0, const Color& c1, const Color& c2, u32 flags = debug::DefaultDrawFlags);

// 行列の描画
//!	@param	[in]	m		行列
//! @param	[in]	scale	表示スケール(default:1.0f)
void drawMatrix(const matrix& m, f32 scale = 1.0f, u32 flags = debug::DefaultDrawFlags);

// ボーンの描画
//!	@param	[in]	p0		親の座標
//! @param	[in]	p1		子の座標
//! @param	[in]	c		カラー
void drawBone(const float3 p0, const float3 p1, const Color& c, u32 flags = debug::DefaultDrawFlags);

//! 矢印の描画
//! @param  [in]    position    座標
//! @param  [in]    dir         方向(長さも影響)
//! @param  [in]    color       カラー
//! @param  [in]    flags       フラグ(debug::DRAW_FLAGの組み合わせ)
void drawArrow(const float3& position, const float3& dir, const Color& color, u32 flags = debug::DefaultDrawFlags);

//! 法線の描画
//! @param  [in]    position    座標
//! @param  [in]    normal      法線
//! @param  [in]    color       カラー
//! @param  [in]    flags       フラグ(debug::DRAW_FLAGの組み合わせ)
void drawNormal(const float3& position, const float3& normal, const Color& color, u32 flags = debug::DefaultDrawFlags);

//! 球の描画
//! @param  [in]    position    座標
//! @param  [in]    radius      半径
//! @param  [in]    color       カラー
//! @param  [in]    flags       フラグ(debug::DRAW_FLAGの組み合わせ)
void drawSphere(const float3& position, f32 radius, const Color& color, u32 flags = debug::DefaultDrawFlags);

}   // namespace debug
