﻿//---------------------------------------------------------------------------
//!	@file	dx11_swap_chain.h
//!	@brief	スワップチェイン
//---------------------------------------------------------------------------
#pragma once

namespace dx11 {

//===========================================================================
//! スワップチェイン
//===========================================================================
class SwapChain
{
public:
    virtual ~SwapChain() = default;

    //! 画面更新
    //! @param  [in]    vsyncInterval   VSYNCの間隔(0:ノーウェイト 1:60fps 2:30fps 3:20fps 4:15fps)
    virtual bool present(u32 vsyncInterval) = 0;

    //! GPUが実行完了になるまで待つ
    //! 完全にアイドル状態になるまでブロックします
    virtual void waitForGpu() = 0;

    //! バックバッファのサイズ変更
    virtual void resizeBuffers(u32 width, u32 height) = 0;

    //----------------------------------------------------------
    //! @name 参照
    //----------------------------------------------------------
    //@{

    //! DXGIスワップチェインを取得
    virtual IDXGISwapChain3* dxgiSwapChain() const = 0;

    //! バックバッファを取得
    virtual dx11::Texture* backBuffer() const = 0;

    //@}
};

//! スワップチェインの作成
//! @param  [in]    width       幅
//! @param  [in]    height      高さ
//! @param  [in]    dxgiFormat  ピクセル形式
//! @param  [in]    bufferCount バッファ数(2 or 3)
[[nodiscard]] std::unique_ptr<dx11::SwapChain> createSwapChain(u32 width, u32 height, DXGI_FORMAT dxgiFormat, u32 bufferCount);

}   // namespace dx11
