﻿//---------------------------------------------------------------------------
//!	@file	dx11_buffer.h
//!	@brief	GPUバッファ
//---------------------------------------------------------------------------
#pragma once

namespace dx11 {

//===========================================================================
//! GPUバッファ
//===========================================================================
class Buffer
{
public:
    //----------------------------------------------------------
    //! バッファ情報
    //----------------------------------------------------------
    struct Desc
    {
        size_t      size_         = 0;                     //!< サイズ(単位:bytes)
        u32         d3dBindFlags_ = 0;                     //!< テクスチャの設定可能スロット
        D3D11_USAGE d3dUsage_     = D3D11_USAGE_DEFAULT;   //!< テクスチャの配置場所(メインメモリ or ビデオメモリ)
    };

    //! メモリマッピング開始
    //! GPUメモリをCPU側から参照できるようにします。
    //! @param  [in]    mapType マップの動作タイプ
    virtual void* map(D3D11_MAP mapType = D3D11_MAP_WRITE_DISCARD) = 0;

    //! メモリマッピング終了
    //! GPU実行時に対象のバッファがマップされているとストールします
    //!必ずバッファのマッピングを解除してから発行します。
    virtual void unmap() = 0;

    //! バッファへ更新
    template<typename T>
    void update(const T& source)
    {
        //ちょっと問題がある
        //ASSERT_MESSAGE(desc().size_ < sizeof(source), "サイズが大きすぎます.");

        auto p = map();
        memcpy(p, &source, sizeof(source));
        unmap();
    }
    //----------------------------------------------------------
    //! @name キャスト
    //----------------------------------------------------------
    //@{

    //! ID3D11Buffer参照
    virtual operator ID3D11Buffer*() const = 0;

    //@}
    //----------------------------------------------------------
    //! @name 参照
    //----------------------------------------------------------
    //@{

    //! バッファ情報を取得
    virtual const Buffer::Desc& desc() const = 0;

    //! D3Dリソースを取得
    virtual ID3D11Resource* d3dResource() const = 0;

    //@}
protected:
    virtual ~Buffer() = default;
};

//! バッファを作成
//! @param  [in]    desc              オプション設定
//! @param  [in]    initializeData    初期化用のデーター(省略する場合はnullptrを指定)
std::shared_ptr<dx11::Buffer> createBuffer(const Buffer::Desc& desc, const void* initializeData = nullptr);

//! D3Dリソースから作成
//! @param  [in]    d3dResource 対象のD3Dリソース
//! @note 引数のリソースは内部で参照カウンタで所有します。
std::shared_ptr<dx11::Buffer> createBuffer(ID3D11Resource* d3dResource);

//----------------------------------------------------------------------
//! @defgroup   ユーティリティー
//! 引数を簡略化した生成関数
//----------------------------------------------------------------------
//@{

//! バッファ作成の固定配列サイズ自動計算つき作成 (ビデオメモリ側に配置/初期化以降変更不可な静的なバッファ)
//! @param  array           配列
//! @param  d3dBindFlags    テクスチャの設定可能スロット
//! 【使用例】
//! buffer = dx11::createBuffer(配列, D3D11_BIND_VERTEX_BUFFER);
template<typename T, int N>
std::shared_ptr<dx11::Buffer> createBuffer(const T (&array)[N], u32 d3dBindFlags)
{
    return dx11::createBuffer({ sizeof(T) * N, d3dBindFlags, D3D11_USAGE_IMMUTABLE }, array);
}

//! バッファ作成の型指定サイズ自動計算つき作成 (メインメモリ側に配置/動的なバッファ)
//! @param  T               バッファの型
//! @param  d3dBindFlags    テクスチャの設定可能スロット
//! 【使用例】
//! buffer = dx11::createDynamicBuffer<CameraInfo>(D3D11_BIND_CONSTANT_BUFFER);
//! 主に動的に変更する定数バッファに有効です
template<typename T>
std::shared_ptr<dx11::Buffer> createDynamicBuffer(u32 d3dBindFlags)
{
    return dx11::createBuffer({ sizeof(T), d3dBindFlags, D3D11_USAGE_DYNAMIC });
}

//@}

}   // namespace dx11
