﻿//---------------------------------------------------------------------------
//!	@file	dx11_shader.h
//!	@brief	GPUシェーダー
//---------------------------------------------------------------------------
#pragma once

namespace dx11 {

class Buffer;

//===========================================================================
//! シェーダー
//===========================================================================
class Shader
{
public:
    // 定数バッファとスロット番号のペア
    using ConstantBufferInfo = std::pair<std::shared_ptr<dx11::Buffer>, u32>;

    //----------------------------------------------------------
    //! @name キャスト
    //----------------------------------------------------------
    //@{

    virtual operator ID3D11VertexShader*() const   = 0;
    virtual operator ID3D11PixelShader*() const    = 0;
    virtual operator ID3D11GeometryShader*() const = 0;
    virtual operator ID3D11HullShader*() const     = 0;
    virtual operator ID3D11DomainShader*() const   = 0;
    virtual operator ID3D11ComputeShader*() const  = 0;

    //@}
    //----------------------------------------------------------
    //! @name 参照
    //----------------------------------------------------------
    //@{

    //! シェーダーバイトコードを取得
    virtual const void* byteCode() const = 0;

    //! シェーダーバイトコードのサイズを取得
    virtual size_t byteCodeSize() const = 0;

    //! ハッシュ値を取得
    virtual u64 hash() const = 0;

    //! 定数バッファリストを取得
    virtual const std::vector<Shader::ConstantBufferInfo>* constantBufferList() const=0;

    //@}
protected:
    virtual ~Shader() = default;
};

//! シェーダーをコンパイル
//! @param  [in]    path    ファイルパス
//! @param  [in]    entrypoint    関数名
//! @param  [in]    target    シェーダーモデル名 "vs_5_0" "ps_5_0" etc...
[[nodiscard]] std::shared_ptr<dx11::Shader> createShader(const char* path, const char* entrypoint, const char* target);

}   // namespace dx11
