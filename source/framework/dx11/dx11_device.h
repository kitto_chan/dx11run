﻿//---------------------------------------------------------------------------
//!	@file	dx11_device.h
//!	@brief	GPUデバイス
//---------------------------------------------------------------------------
#pragma once

namespace dx11 {

class Device
{
public:
    Device()          = default;
    virtual ~Device() = default;

    //! 初期化
    bool initialize(u32 width, u32 height);

    //! D3D11デバイスを取得
    ID3D11Device5* d3dDevice() const { return d3d11Device_.Get(); }

    //! DXGIFactoryを取得
    IDXGIFactory4* dxgiFactory() const { return dxgiFactory_.Get(); }

    //! 直接コンテキストを取得
    ID3D11DeviceContext4* immediateContext() const { return d3dImmediateContext_.Get(); }

private:
    // コピー禁止/move禁止
    Device(const Device&) = delete;
    Device(Device&&)      = delete;
    Device& operator=(const Device&) = delete;
    Device& operator=(Device&&) = delete;

private:
    D3D_DRIVER_TYPE   driverType_;     //!< 作成されたデバイスの種類	    (HW, WARP, REF)
    D3D_FEATURE_LEVEL featureLevel_;   //!< 作成されたデバイスの機能レベル (DX9,DX10,DX11)

    com_ptr<ID3D11Device5>        d3d11Device_;           //!< D3D11デバイス
    com_ptr<ID3D11DeviceContext4> d3dImmediateContext_;   //!< 直接コンテキスト
    com_ptr<IDXGIFactory4>        dxgiFactory_;           //!< DXGIFactory
};

}   // namespace dx11