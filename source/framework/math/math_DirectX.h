﻿//---------------------------------------------------------------------------
//!	@file	math_DirectX.h
//!	@brief	cast hlslpp <-> DirectX
//---------------------------------------------------------------------------
#pragma once
namespace math {
DirectX::XMFLOAT4 cast(const float4& f4);   //!< hlslpp::float4 -> DirectX::XMFLOAT4
DirectX::XMFLOAT3 cast(const float3& f3);   //!< hlslpp::float3 -> DirectX::XMFLOAT3

float4 cast(const DirectX::XMFLOAT4& f4);   //!< DirectX::XMFLOAT4 -> hlslpp::float4
float3 cast(const DirectX::XMFLOAT3& f3);   //!< DirectX::XMFLOAT3 -> hlslpp::float3

DirectX::XMVECTOR castXMVector(const float4& f4); //!< hlslpp::float4 -> DirectX::XMVECTOR
}   // namespace math
