﻿//---------------------------------------------------------------------------
//!	@file	math_random.h
//!	@brief	ランダムのヘルパークラス
//---------------------------------------------------------------------------
#pragma once

namespace math {
//! 0 ~ i_max までのランダムな値の返す関数（ int 型）
s32 GetRandomI(int i_max);
//! i_from ～ i_to までのランダムな値を返す関数（ int 型）
s32 GetRandomI(int i_from, int i_to);
//!	0.0 ~ 1.0 までのランダムな値を返す関数（ float 型）
f32 GetRandomF();
//!	0.0 ~ f_max までのランダムな値を返す（ float 型）
f32 GetRandomF(float f_max);
//!	f_from ~ f_to までのランダムな値を返す（ float 型）
f32 GetRandomF(float f_from, float f_to);
}   // namespace math
