﻿#pragma once
namespace geometry {
struct VertexData
{
    DirectX::XMFLOAT3 pos;   //! 座標(position)
    DirectX::XMFLOAT3 nor;   //! ノーマル(norma)
    DirectX::XMFLOAT4 tan;   //! 正接(tangent)
    DirectX::XMFLOAT4 col;   //! 色 (color)
    DirectX::XMFLOAT2 tex;   //! テクスチャ座標系 (tex coord)
};

struct MeshData
{
    std::vector<VertexData> vertexData;
    std::vector<int>        indexData;
};

MeshData CreateCphere(f32 rad, u32 levels = 20, u32 slices = 20, const float4& color = { 1.0f, 1.0f, 1.0f, 1.0f });
MeshData CreateBox(const float3& size, const float4& color = { 1.0f, 1.0f, 1.0f, 1.0f });
}   // namespace gpu_util
