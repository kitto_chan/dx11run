﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.cpp
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#include "imgui/imgui.h"
#include "ImguiWidget.h"
#include "../app/component/Transform.h"   // TODO: better handle..

namespace ui {
//---------------------------------------------------------------------------
//! float3 を Imgui DragFloatの形で描画する
//---------------------------------------------------------------------------
void ImguiDragXYZ(const std::string& label, float3& xyz)
{
    f32 x = xyz.x;
    f32 y = xyz.y;
    f32 z = xyz.z;

    ImGui::Text(label.c_str());
    ImGui::PushItemWidth(50);

    ImGui::PushID(label.c_str());
    ImGui::DragFloat("X", &x);
    ImGui::SameLine();
    ImGui::PopID();

    ImGui::PushID(label.c_str());
    ImGui::DragFloat("Y", &y);
    ImGui::SameLine();
    ImGui::PopID();

    ImGui::PushID(label.c_str());
    ImGui::DragFloat("Z", &z);
    ImGui::PopID();

    xyz.x = x;
    xyz.y = y;
    xyz.z = z;
    ImGui::PopItemWidth();
}
//---------------------------------------------------------------------------
//! ImguiGuizmoViewを描画する
//---------------------------------------------------------------------------
void ImguiGuizmoView(const matrix& viewMat, const matrix& projMat, const raw_ptr<component::Transform>& transCom)
{
    if(!transCom) return;

    ImGui::Begin("Scene");
    ImGuizmo::SetOrthographic(false);
    ImGuizmo::SetDrawlist(ImGui::GetForegroundDrawList());
    ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y,
                      ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y);

    matrix worldMat = transCom->GetLocalToWorldMatrix();

    // cast matrix -> float[16]
    float* viewF16  = math::CastF16(viewMat);
    float* projF16  = math::CastF16(projMat);
    float* worldF16 = math::CastF16(worldMat);

    switch(SystemSettingsMgr()->GetImGuizmoAction()) {
        case sys::ImGuizmoAction::Transform:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::TRANSLATE,
                                 ImGuizmo::LOCAL, worldF16);
            break;
        case sys::ImGuizmoAction::Rotate:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::ROTATE,
                                 ImGuizmo::LOCAL, worldF16);
            break;
        case sys::ImGuizmoAction::Scale:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::SCALE,
                                 ImGuizmo::LOCAL, worldF16);
            break;
        default:
            ASSERT_MESSAGE(false, "不可能なEnum");
            break;
    }

    if(ImGuizmo::IsUsing()) {
        float matrixTranslation[3], matrixRotation[3], matrixScale[3];
        ImGuizmo::DecomposeMatrixToComponents(worldF16, matrixTranslation, matrixRotation, matrixScale);

        // 回転はオイラー角保存してるのでへ
        float3 rot = transCom->GetRotation();
        rot.x      = math::D2R(matrixRotation[0]);
        rot.y      = math::D2R(matrixRotation[1]);
        rot.z      = math::D2R(matrixRotation[2]);

        transCom->SetPosition(float3(matrixTranslation[0], matrixTranslation[1], matrixTranslation[2]));
        transCom->SetScale(float3(matrixScale[0], matrixScale[1], matrixScale[2]));
        //empty->GetComponent<component::Transform>()->SetRotation(float3(matrixRotation[0], matrixRotation[1], matrixRotation[2]));
        transCom->SetRotation(rot);
    }
    ImGui::End();
}
void ImguiTopBar()
{
    ImGui::Begin("Actions");
    static std::string type[3] = { "Transform", "Rotation", "Scale" };

    s32 selected   = static_cast<s32>(SystemSettingsMgr()->GetImGuizmoAction());
    f32 alignRight = ImGui::GetWindowSize().x * 0.8f;
    ImGui::SameLine(alignRight);
    ImGui::Text("");
    for(int n = 0; n < 3; n++) {
        ImGui::SameLine();
        if(ImGui::Selectable(type[n].c_str(), selected == n, ImGuiSelectableFlags_None, ImVec2(80, 10)))
            SystemSettingsMgr()->SetImGuizmoAction(sys::ImGuizmoAction(n));
    }
    ImGui::End();
}
}   // namespace ui
