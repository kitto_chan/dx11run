﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.h
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#pragma once

namespace component {
class Transform;
}
namespace ui {
//! float3 を Imgui DragFloatの形で描画する
void ImguiDragXYZ(const std::string& label, float3& xyz);
//! ImguiGuizmoViewを描画する
void ImguiGuizmoView(const matrix& view, const matrix& proj, const raw_ptr<component::Transform>& transCom);
//! トップバーを描画
void ImguiTopBar();
}   // namespace ui
