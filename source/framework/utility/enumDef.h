﻿//---------------------------------------------------------------------------
//!	@file	enumDef.h
//!	@brief	型定義
//---------------------------------------------------------------------------

namespace sys {
// モード
enum class SystemMode
{
    EditorMode,   //!< クリエイトモード（開発用）
    GameMode      //!< ゲームモード    　(クライアント側)
};

// ImGuizmoのアクション
enum class ImGuizmoAction
{
    Transform,   //!< 変換座標
    Rotate,      //!< 回転
    Scale        //!< スケール
};

}   // namespace sys
