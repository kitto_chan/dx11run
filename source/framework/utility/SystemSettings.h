﻿//---------------------------------------------------------------------------
//!	@file	SystemSettings.h
//!	@brief	ゲーム設定
//---------------------------------------------------------------------------
#pragma once
namespace manager {
using namespace sys;
class SystemSettings : public Singleton<SystemSettings>
{
public:
    SystemSettings()  = default;
    ~SystemSettings() = default;

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------
    ImGuizmoAction GetImGuizmoAction() const { return _imguizmoAction; };
    void           SetImGuizmoAction(const ImGuizmoAction& set) { _imguizmoAction = set; };

    SystemMode GetSysMode() const { return _sysMode; }
    void       SwapMode() { _sysMode == SystemMode::EditorMode ?
                                _sysMode = SystemMode::GameMode :
                                _sysMode = SystemMode::EditorMode; }
    bool       IsEditorMode() const { return _sysMode == SystemMode::EditorMode; }

private:
    //---------------------------------------------------------------------------
    //! 内部変数
    //---------------------------------------------------------------------------
    ImGuizmoAction _imguizmoAction = ImGuizmoAction::Transform;
    SystemMode     _sysMode        = SystemMode::GameMode;
};

}   // namespace manager
manager::SystemSettings* SystemSettingsMgr();
