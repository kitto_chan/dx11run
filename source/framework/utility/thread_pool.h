﻿//---------------------------------------------------------------------------
//!	@file	thread_pool.h
//! @brief	スレッドプール
//---------------------------------------------------------------------------
#pragma once

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <atomic>

//===========================================================================
//! スレッドプール
//===========================================================================
class ThreadPool
{
public:
    //! コンストラクタ
    //! @param  [in]    threadCount ワーカースレッド数(default:CPUの論理スレッド数)
    ThreadPool(std::size_t threadCount = std::thread::hardware_concurrency());

    //! デストラクタ
    virtual ~ThreadPool();

    //! ジョブの追加
    void submit(std::function<void(void)> job);

    //! ワーカースレッドの解放と終了
    //! @param  [in]    waitForAll  ジョブの実行完了を待機してから解放するかどうか
    //! @note   この呼び出しの後にキューは空になりスレッドは終了します。
    //! @note   join()を実行した後はスレッドプールを使用することはできません。
    //! @note   ジョブの完了後もプールを存続させる必要がある場合は、wait() を利用します
    void join(bool waitForAll = true);

    //! ジョブの実行完了を待機
    void wait();

    //! ワーカースレッドの数を取得
    size_t size() const;

private:
    // コピー禁止/move禁止
private:
    std::vector<std::thread>              threads_;   //!< ワーカースレッド
    std::queue<std::function<void(void)>> queue_;     //!< ジョブキュー

    std::atomic<s32>        jobsLeft_  = 0;       //!< 残りのジョブ数
    std::atomic<bool>       isBailout_ = false;   //!< 終了リクエスト
    std::atomic<bool>       isFnished_ = false;   //!< ジョブ実行完了フラグ
    std::condition_variable jobAvailableVar_;     //!< 条件変数
    std::condition_variable waitVar_;             //!< 待機用条件変数
    std::mutex              waitMutex_;           //!< 待機用mutex
    std::mutex              queueMutex_;          //!< キュー用mutex
};

//! スレッドプールを取得
ThreadPool* threadPool();
