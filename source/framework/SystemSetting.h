﻿//---------------------------------------------------------------------------
//! @file    SystemSettings.h
//! @brief   システム設定
//---------------------------------------------------------------------------
#pragma once
namespace sysst {
constexpr s32 WND_W        = 1440;
constexpr s32 WND_H        = 810;
constexpr f32 ASPECT_RATIO = static_cast<f32>(WND_W) / WND_H;

constexpr bool FULLSCREEN_MODE = false;
constexpr char GAME_TITLE[]    = "BaseEngine";
}   // namespace sysst