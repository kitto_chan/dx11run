﻿////---------------------------------------------------------------------------
////!	@file	sample_collision.cpp
////!	@brief	衝突判定サンプル
////---------------------------------------------------------------------------
//#include "sample_collision.h"
//#include "renderer/model.h"
//namespace scene {
//
//namespace {
//
////--------------------------------------------------------------
//// 内部変数
////--------------------------------------------------------------
//
////--------
//// モデル描画
//std::shared_ptr<Model> modelBox_;       //!< モデル(木箱)
//std::shared_ptr<Model> modelBall_;      //!< モデル(サッカーボール)
//std::shared_ptr<Model> modelOilDrum_;   //!< モデル(ドラム缶)
//std::shared_ptr<Model> modelGround_;    //!< モデル(地形)
//CameraDebug            camera_;         //!< デバッグカメラ
//
////--------
//// 物理シミュレーション
//std::unique_ptr<physics::RigidBody>              groundRigidBody_;   //!< 地面用の剛体
//std::vector<std::unique_ptr<physics::RigidBody>> rigidBodies_;       //!< 剛体
//std::unique_ptr<physics::Character>              playerCharacter_;   //!< プレイヤーキャラクター
//
////--------
//// フラグ類
//bool isDrawDebug_ = false;   //!< 物理シミュレーションエンジンデバッグ描画
//
////--------------------------------------------------------------
//// シーン用定数バッファ
//// 定数バッファ構造体はシェーダー側と同じデーター構造にする必要あり
////--------------------------------------------------------------
//struct CameraCB
//{
//    matrix matView_ = math::identity();   //!< ビュー行列
//    matrix matProj_ = math::identity();   //!< 投影行列
//};
//
//CameraCB cameraCb_{};   //!< カメラ用定数バッファ
//
//}   // namespace
//
////--------------------------------------------------------------------------
////! デストラクタ
////--------------------------------------------------------------------------
//SceneCollision::~SceneCollision()
//{
//    //==========================================================
//    // 物理シミュレーション解放
//    //==========================================================
//    groundRigidBody_.reset();
//    rigidBodies_.clear();
//    playerCharacter_.reset();
//}
//
////--------------------------------------------------------------------------
////! 初期化
////--------------------------------------------------------------------------
//bool SceneCollision::Init()
//{
//    //-------------------------------------------------------------
//    // モデルの読み込み
//    //-------------------------------------------------------------
//    if(!modelBox_) {
//        modelBox_ = createModel("wooden_box/wooden_box.fbx");
//    }
//    if(!modelBall_) {
//        modelBall_ = createModel("soccer_ball/SOCCER_Ball.FBX");
//    }
//    if(!modelOilDrum_) {
//        modelOilDrum_ = createModel("oil_drum/Oil_Drum.fbx");
//    }
//    if(!modelGround_) {
//        modelGround_ = createModel("street/Street environment_V01.FBX");
//    }
//
//    //----------------------------------------------------------
//    // カメラの初期位置設定
//    //----------------------------------------------------------
//    camera_.setPosition(float3(-5.0f, 2.0f, -5.0f));   // 位置座標
//    camera_.setLookAt(float3(-10.0f, 2.0f, -10.0f));   // 注視点
//
//    //==========================================================
//    // 物理シミュレーション初期化
//    //==========================================================
//    auto createRigidBody = [](const matrix& matWorld) {
//        std::unique_ptr<physics::RigidBody> rigidBody;
//
//        // 種類をランダムに生成
//        switch(rand() % 3) {
//            case 0:
//            {
//                // 木箱
//                constexpr f32 mass     = 15.0f;   // 質量 15kg【動的オブジェクト】
//                constexpr f32 halfSize = 0.38f;   // 木箱サイズ
//
//                const auto shape = shape::Box(float3(halfSize, halfSize, halfSize));   // 形状
//
//                rigidBody = physics::createRigidBody(mass, matWorld, shape);
//                rigidBody->nativeRigidBody()->setFriction(0.5f);      // 摩擦係数
//                rigidBody->nativeRigidBody()->setRestitution(0.2f);   // 跳ね返り係数
//            } break;
//            case 1:
//            {
//                // サッカーボール
//                constexpr f32 massFactor = 4.0f;                // 安定のために意図的に重くする
//                constexpr f32 mass       = 0.5f * massFactor;   // 質量 500g【動的オブジェクト】
//
//                const auto shape = shape::Sphere(0.12f);   // 形状
//
//                rigidBody = physics::createRigidBody(mass, matWorld, shape);
//                rigidBody->nativeRigidBody()->setFriction(0.25f);           // 摩擦係数
//                rigidBody->nativeRigidBody()->setRestitution(0.9f);         // 跳ね返り係数(ボール用に少し高めに設定)
//                rigidBody->nativeRigidBody()->setRollingFriction(0.01f);    // 転がり摩擦
//                rigidBody->nativeRigidBody()->setSpinningFriction(0.01f);   // スピン摩擦
//            } break;
//            case 2:
//            {
//                // ドラム缶
//                constexpr f32 mass             = 20.0f;    // 質量 20kg【動的オブジェクト】
//                constexpr f32 radius           = 0.375f;   // 半径
//                constexpr f32 heightHalfExtent = 0.54f;    // 高さの半分
//
//                const auto shape = shape::Cylinder(radius, heightHalfExtent);   // 形状
//
//                rigidBody = physics::createRigidBody(mass, matWorld, shape);
//                rigidBody->nativeRigidBody()->setFriction(0.25f);           // 摩擦係数
//                rigidBody->nativeRigidBody()->setRestitution(0.75f);        // 跳ね返り係数
//                rigidBody->nativeRigidBody()->setRollingFriction(0.01f);    // 転がり摩擦
//                rigidBody->nativeRigidBody()->setSpinningFriction(0.01f);   // スピン摩擦
//            } break;
//        }
//        return rigidBody;
//    };
//
//    //-------------------------------------------------------
//    // 複数の剛体を並べて作成
//    //-------------------------------------------------------
//    constexpr s32 ARRAY_SIZE_X = 3;
//    constexpr s32 ARRAY_SIZE_Y = 4;
//    constexpr s32 ARRAY_SIZE_Z = 7;
//    for(s32 i = 0; i < ARRAY_SIZE_X; i++) {
//        for(s32 j = 0; j < ARRAY_SIZE_Y; j++) {
//            for(s32 k = 0; k < ARRAY_SIZE_Z; k++) {
//                f32 x = static_cast<f32>(i - ARRAY_SIZE_X / 2);
//                f32 y = static_cast<f32>(j);
//                f32 z = static_cast<f32>(k - ARRAY_SIZE_Z / 2);
//
//                // 初期座標
//                float3 position;
//                position.x = 1.2f * x + -20.0f;
//                position.y = 1.2f * y + 8.0f;
//                position.z = 1.2f * z + -15.0f;
//
//                // ランダムで位置をずらす
//                position.x += static_cast<f32>(rand() & 7) * 0.02f;
//                position.z += static_cast<f32>(rand() & 7) * 0.02f;
//
//                //--------------------------------------------------------------
//                // 剛体を作成してリストに登録
//                //--------------------------------------------------------------
//                const matrix matWorld = math::translate(position);   // 初期姿勢
//
//                std::unique_ptr<physics::RigidBody> rigidBody = createRigidBody(matWorld);
//
//                rigidBodies_.emplace_back(std::move(rigidBody));
//            }
//        }
//    }
//
//    //----------------------------------------------------------
//    // 地面を生成
//    //----------------------------------------------------------
//    {
//        constexpr f32 mass = 0.0f;   // 【静的オブジェクト】
//
//        // モデルメッシュから剛体を生成
//        groundRigidBody_ = physics::createRigidBody(mass, math::identity(), modelGround_);
//
//        groundRigidBody_->nativeRigidBody()->setFriction(0.9f);   // 摩擦係数
//    }
//
//    //==========================================================
//    // プレイヤーの衝突判定用のキャラクターを作成
//    //==========================================================
//    {
//        constexpr f32 radius   = 0.5f;                                            // 球の半径
//        const matrix  matWorld = math::translate(float3(-10.0f, 2.0f, -10.0f));   // 初期位置
//
//        playerCharacter_ = physics::createCharacter(matWorld, radius);
//        if(!playerCharacter_) {
//            return false;
//        }
//    }
//
//    return true;
//}
//
////--------------------------------------------------------------------------
////! 更新
////! @attention ここでは描画発行は行わないこと
////--------------------------------------------------------------------------
//void SceneCollision::Update([[maybe_unused]] f32 deltaTime)
//{
//    // カメラ行列
//    camera_.update(deltaTime);
//    cameraCb_.matView_ = camera_.view();
//    cameraCb_.matProj_ = camera_.projection();
//
//    //==========================================================
//    // プレイヤー操作
//    //==========================================================
//    float3 dir(0.0f, 0.0f, 0.0f);
//
//    if(GetKeyState(VK_LEFT) & 0x8000) {   // ←キー
//        dir.x -= 1.0f;
//    }
//    if(GetKeyState(VK_RIGHT) & 0x8000) {   // →キー
//        dir.x += 1.0f;
//    }
//
//    if(GetKeyState(VK_UP) & 0x8000) {   // ↑キー
//        dir.z -= 1.0f;
//    }
//    if(GetKeyState(VK_DOWN) & 0x8000) {   // ↓キー
//        dir.z += 1.0f;
//    }
//
//    // 速度を統一
//    if(dot(dir, dir).x > 0.0f) {
//        dir = normalize(dir) * 0.02f;
//    }
//
//    //--------
//    // 移動
//    playerCharacter_->walk(dir);
//
//    //--------
//    // ジャンプ
//    {
//        static bool lastKey = false;
//        bool        key     = GetKeyState(' ') & 0x8000;
//
//        // 押した瞬間
//        if(key && !lastKey) {
//            if(playerCharacter_->canJump()) {
//                playerCharacter_->jump(float3(0.0f, 6.0f, 0.0f));
//            }
//        }
//
//        lastKey = key;   // 次のフレームのために保存
//    }
//
//    //----------------------------------------------------------
//    // 3Dモデルのアニメーションを更新
//    //----------------------------------------------------------
//    modelBox_->update();
//    modelBall_->update();
//    modelOilDrum_->update();
//    modelGround_->update();
//
//    //----------------------------------------------------------
//    // ImGui ウィンドウ表示テスト
//    //----------------------------------------------------------
//    ImGui::SetNextWindowSize(ImVec2(320, 180), ImGuiCond_Once);
//    ImGui::Begin(u8"物理シミュレーション");
//    {
//        ImGui::Text(u8"【キャラクター操作サンプル】");
//        ImGui::Text(u8"上下左右キー : 移動");
//        ImGui::Text(u8"SPACE : ジャンプ");
//        ImGui::Separator();
//
//        ImGui::Checkbox(u8"物理シミュレーションエンジンデバッグ描画", &isDrawDebug_);
//    }
//    ImGui::End();
//}
//
////--------------------------------------------------------------------------
////! 描画
////! @attention ここでは移動更新は行わないこと
////--------------------------------------------------------------------------
//void SceneCollision::Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
//{
//    //----------------------------------------------------------
//    // 描画バッファ設定
//    //----------------------------------------------------------
//    gpu::setRenderTarget(0, colorTexture);
//    gpu::setDepthStencil(depthTexture);
//
//    // カラーとデプスバッファをクリア
//    gpu::clearColor(colorTexture, float4(0.5f, 0.5f, 0.8f, 1.0f));   // カラーバッファ
//    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ
//
//    // 表裏カリングOFF
//    gpu::setRasterizerState(dx11::commonStates().CullNone());
//
//    // カメラ定数バッファ
//    gpu::setConstantBuffer("CameraCB", cameraCb_);
//
//    //----------------------------------------------------------
//    // 軸の描画
//    //----------------------------------------------------------
//    {
//        constexpr f32 gridSize = 64.0f;   // グリッドの大きさ
//
//        // 軸の描画
//        debug::drawLineF(float3(-gridSize, 0.0f, 0.0f), float3(+gridSize, 0.0f, 0.0f), Color(255, 0, 0));   // X軸
//        debug::drawLineF(float3(0.0f, -gridSize, 0.0f), float3(0.0f, +gridSize, 0.0f), Color(0, 255, 0));   // Y軸
//        debug::drawLineF(float3(0.0f, 0.0f, -gridSize), float3(0.0f, 0.0f, +gridSize), Color(0, 0, 255));   // Z軸
//    }
//
//    //==========================================================
//    // モデルを描画
//    //==========================================================
//    {
//        // 背景モデル
//        auto matWorld = math::translate(float3(0.0f, 0.0f, 0.0f));
//
//        modelGround_->setWorldMatrix(matWorld);
//        modelGround_->render();   // 描画
//    }
//
//    //----------------------------------------------------------
//    // 剛体モデルの描画
//    //----------------------------------------------------------
//    for(auto& rigidBody : rigidBodies_) {
//        const matrix matWorld = rigidBody->worldMatrix();   // 剛体からワールド行列を取得
//
//        switch(rigidBody->shapeType()) {
//            case SPHERE_SHAPE_PROXYTYPE:
//            {
//                // [Sphere] サッカーボールモデル
//                modelBall_->setWorldMatrix(matWorld);
//                modelBall_->render();
//            } break;
//            case BOX_SHAPE_PROXYTYPE:
//            {
//                // [Box] 木箱モデル
//                modelBox_->setWorldMatrix(matWorld);
//                modelBox_->render();
//            } break;
//            case CYLINDER_SHAPE_PROXYTYPE:
//            {
//                // [Cylinder] ドラム缶
//                matrix m = mul(math::translate(float3(0.0f, -0.54f, 0.0f)), matWorld);   // 表示位置をずらす調整
//                modelOilDrum_->setWorldMatrix(m);
//                modelOilDrum_->render();
//            } break;
//            default: break;
//        }
//    }
//
//    //==========================================================
//    // プレイヤーを描画
//    //==========================================================
//    {
//        auto matWorld = playerCharacter_->worldMatrix();
//        auto position = math::makeTranslation(matWorld);   // 位置
//
//        // 緑色の球を描画
//        debug::drawSphere(position, 0.5f, Color(0, 255, 0));
//    }
//
//    //==========================================================
//    // 物理シミュレーションエンジンのデバッグ描画
//    //==========================================================
//    if(isDrawDebug_) {
//        physics::PhysicsEngine::instance()->renderDebug();
//    }
//}
//
////---------------------------------------------------------------------------
//// シーンの生成
////! @note テンプレートの特殊化で実装します
////---------------------------------------------------------------------------
//template<>
//std::unique_ptr<IScene> createScene<SceneCollision>()
//{
//    return std::make_unique<SceneCollision>();
//}
//}   // namespace scene
