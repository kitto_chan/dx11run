﻿////---------------------------------------------------------------------------
////!	@file	sample_physics.h
////!	@brief	物理シミュレーションサンプル
////---------------------------------------------------------------------------
//#pragma once
//namespace scene {
//
////===========================================================================
////! 物理シミュレーションサンプル用シーン
////===========================================================================
//class ScenePhysics final : public IScene
//{
//public:
//    //! コンストラクタ
//    ScenePhysics() = default;
//
//    //! デストラクタ
//    virtual ~ScenePhysics();
//
//    //! 初期化
//    virtual bool Init() override;
//
//    //! 更新
//    //! @param  [in]    deltaTime   進行時間(単位:秒)
//    virtual void Update(f32 deltaTime) override;
//
//    //! 描画
//    virtual void Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;
//};
//}   // namespace scene