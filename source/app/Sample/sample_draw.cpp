﻿////---------------------------------------------------------------------------
////!	@file	sample_draw.cpp
////!	@brief	描画サンプル
////---------------------------------------------------------------------------
//#include "sample_draw.h"
//namespace scene {
//
//namespace {
//
////--------------------------------------------------------------
//// 頂点
////--------------------------------------------------------------
//struct Vertex
//{
//    DirectX::XMFLOAT3 position_;   //!< POSITION    座標
//    DirectX::XMFLOAT2 uv_;         //!< TEXCOORD    テクスチャ座標
//};
//
////--------------------------------------------------------------
//// 内部変数
////--------------------------------------------------------------
//CameraDebug camera_;   //!< デバッグカメラ
//
//// シェーダー
//std::shared_ptr<gpu::Shader> shaderVs_3D_;        //!< VS 3D頂点シェーダー
//std::shared_ptr<gpu::Shader> shaderPs_Texture_;   //!< PS テクスチャありピクセルシェーダー
//std::shared_ptr<gpu::Shader> shaderPs_Flat_;      //!< PS テクスチャなしピクセルシェーダー
//
//// ジオメトリ形状
//std::shared_ptr<gpu::InputLayout> inputLayout_;    //!< 入力レイアウト
//std::shared_ptr<gpu::Buffer>      vertexBuffer_;   //!< 頂点バッファ
//std::shared_ptr<gpu::Buffer>      indexBuffer_;    //!< インデックスバッファ
//
//// テクスチャ(Shader Resource)
//std::shared_ptr<gpu::Texture> texture1_;   //!< テクスチャ1
//std::shared_ptr<gpu::Texture> texture2_;   //!< テクスチャ2
//
////--------------------------------------------------------------
//// シーン用定数バッファ
//// 定数バッファ構造体はシェーダー側と同じデーター構造にする必要あり
////--------------------------------------------------------------
//
//// ワールド行列用定数バッファ
//struct WorldCB
//{
//    matrix matWorld_ = math::identity();   //!< ワールド行列
//};
//
//// カメラ用定数バッファ
//struct CameraCB
//{
//    matrix matView_ = math::identity();   //!< ビュー行列
//    matrix matProj_ = math::identity();   //!< 投影行列
//};
//
//struct ColorCB
//{
//    float4 meshColor_;   //< メッシュの色
//};
//
//WorldCB  worldCb_{};    //!< ワールド用定数バッファ
//CameraCB cameraCb_{};   //!< カメラ用定数バッファ
//ColorCB  colorCb_{};    //!< カラー用定数バッファ
//
//}   // namespace
//
////--------------------------------------------------------------------------
////! 初期化
////--------------------------------------------------------------------------
//bool SceneDraw::Init()
//{
//    //-------------------------------------------------------------
//    // シェーダーをコンパイル
//    //-------------------------------------------------------------
//    shaderVs_3D_      = gpu::createShader("dx11/vs_3d.fx", "main", "vs_5_0");        // 頂点シェーダー
//    shaderPs_Texture_ = gpu::createShader("dx11/ps_texture.fx", "main", "ps_5_0");   // ピクセルシェーダー
//    shaderPs_Flat_    = gpu::createShader("dx11/ps_flat.fx", "main", "ps_5_0");      // ピクセルシェーダー
//    if(!shaderVs_3D_ || !shaderPs_Texture_ || !shaderPs_Flat_)
//        return false;
//
//    //----------------------------------------------------------
//    // 入力レイアウトの作成
//    //----------------------------------------------------------
//    // clang-format off
//    static constexpr D3D11_INPUT_ELEMENT_DESC layout[]{
//        // セマンテック名(任意)                ストリームスロット番号(0-15)
//        // ↓  セマンテック番号(0～7)                    ↓    構造体の先頭からのオフセットアドレス(先頭からnバイト目)
//        // ↓        , ↓,         データ形式          , ↓,    ↓                       , 頂点読み込みの更新周期      , 更新間隔
//        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Vertex, position_), D3D11_INPUT_PER_VERTEX_DATA, 0 },
//        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT   , 0, offsetof(Vertex, uv_)      , D3D11_INPUT_PER_VERTEX_DATA, 0 },
//    };
//    // clang-format on
//
//    inputLayout_ = gpu::createInputLayout(layout, std::size(layout));
//
//    //-------------------------------------------------------------
//    // 頂点データ作成
//    // 入力レイアウトと同じメンバ変数レイアウトの構造体の配列
//    //-------------------------------------------------------------
//    constexpr Vertex vertices[]{
//        { DirectX::XMFLOAT3(-1.0f, +1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f) },
//        { DirectX::XMFLOAT3(+1.0f, +1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 0.0f) },
//        { DirectX::XMFLOAT3(+1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 1.0f) },
//        { DirectX::XMFLOAT3(-1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 1.0f) },
//    };
//    constexpr u32 indices[]{
//        0, 1, 2,
//        2, 3, 0
//    };
//
//    // 頂点バッファの作成
//    vertexBuffer_ = gpu::createBuffer(vertices, D3D11_BIND_VERTEX_BUFFER);
//
//    // インデックスバッファの作成
//    indexBuffer_ = gpu::createBuffer(indices, D3D11_BIND_INDEX_BUFFER);
//
//    //----------------------------------------------------------
//    // テクスチャの読み込み
//    //----------------------------------------------------------
//    texture1_ = gpu::createTextureFromFile("dx11/sample.dds");
//    texture2_ = gpu::createTextureFromFile("dx11/seafloor.dds");
//
//    //----------------------------------------------------------
//    // カメラの初期位置設定
//    //----------------------------------------------------------
//    camera_.setPosition(float3(4.0f, 2.0f, -4.0f));   // 位置座標
//    camera_.setLookAt(float3(0.0f, 0.0f, 0.0f));      // 注視点
//
//    return true;
//}
//
////--------------------------------------------------------------------------
////! 更新
////! @attention ここでは描画発行は行わないこと
////--------------------------------------------------------------------------
//void SceneDraw::Update(f32 deltaTime)
//{
//    //----------------------------------------------------------
//    // 回転テスト
//    //----------------------------------------------------------
//    static f32 t = 0.0f;
//    t += deltaTime * 3.0f;
//
//    // ワールド行列
//    // 原点で「Y軸中心にt度回転」
//    worldCb_.matWorld_ = math::rotateY(t);
//
//    // カメラ行列
//    camera_.update(deltaTime);
//    cameraCb_.matView_ = camera_.view();
//    cameraCb_.matProj_ = camera_.projection();
//
//    //----------------------------------------------------------
//    // カラーをアニメーション
//    //----------------------------------------------------------
//    colorCb_.meshColor_.x = (sinf(t * 0.2f) + 1.0f) * 0.5f;
//    colorCb_.meshColor_.y = (cosf(t * 0.4f) + 1.0f) * 0.5f;
//    colorCb_.meshColor_.z = (sinf(t * 0.8f) + 1.0f) * 0.5f;
//    colorCb_.meshColor_.w = 1.0f;
//}
//
////--------------------------------------------------------------------------
////! 描画
////! @attention ここでは移動更新は行わないこと
////--------------------------------------------------------------------------
//void SceneDraw::Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
//{
//    //----------------------------------------------------------
//    // 描画バッファ設定
//    //----------------------------------------------------------
//    gpu::setRenderTarget(0, colorTexture);
//    gpu::setDepthStencil(depthTexture);
//
//    // カラーとデプスバッファをクリア
//    gpu::clearColor(colorTexture, float4(0.5f, 0.5f, 0.8f, 1.0f));   // カラーバッファ
//    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ
//
//    // 表裏カリングOFF
//    gpu::setRasterizerState(gpu::commonStates().CullNone());
//
//    //==============================================================
//    // 四角形を描画
//    //==============================================================
//
//    // 頂点フォーマットが変更される場合にのみ実行
//    gpu::setInputLayout(inputLayout_);   // 入力レイアウト
//
//    // 頂点データー
//    gpu::setVertexBuffer(0, vertexBuffer_);   // 頂点バッファ
//    gpu::setIndexBuffer(indexBuffer_);        // インデックスバッファ
//
//    // シェーダー
//    gpu::vs::setShader(shaderVs_3D_);        // VS 頂点シェーダー (3D)
//    gpu::ps::setShader(shaderPs_Texture_);   // PS ピクセルシェーダー (テクスチャあり)
//
//    // 必要に応じてシェーダーにパラメーターを設定
//    {
//        // 定数バッファ
//        // シェーダー側の定数バッファ名を指定
//        gpu::setConstantBuffer("WorldCB", worldCb_);
//        gpu::setConstantBuffer("CameraCB", cameraCb_);
//        gpu::setConstantBuffer("ColorCB", colorCb_);
//
//        // テクスチャ
//        gpu::ps::setTexture(0, texture1_);                               // PS(t0) テクスチャ
//        gpu::ps::setSamplerState(0, gpu::commonStates().LinearWrap());   // PS(s0) サンプラー
//    }
//
//    // インデックス指定で三角形描画発行
//    gpu::drawIndexed(gpu::Primitive::TriangleList, 6);   // 6個のインデックス
//
//    //--------------------------------------------------------------
//    // ここ以降はワールド行列を初期化して背景として描画
//    //--------------------------------------------------------------
//    worldCb_.matWorld_ = matrix::identity();
//    gpu::setConstantBuffer("WorldCB", worldCb_);   // GPUへ変更を反映
//
//    //==============================================================
//    // グリッドの描画
//    // メモリ上の配列を直接描画
//    //==============================================================
//    {
//        constexpr f32 gridSize = 64.0f;   // グリッドの大きさ
//
//        constexpr Vertex v[] = {
//            { { -gridSize, 0.0f, 0.0f }, { 0.0f, 0.0f } },
//            { { +gridSize, 0.0f, 0.0f }, { 0.0f, 0.0f } },
//
//            { { 0.0f, -gridSize, 0.0f }, { 0.0f, 0.0f } },
//            { { 0.0f, +gridSize, 0.0f }, { 0.0f, 0.0f } },
//
//            { { 0.0f, 0.0f, -gridSize }, { 0.0f, 0.0f } },
//            { { 0.0f, 0.0f, +gridSize }, { 0.0f, 0.0f } },
//        };
//
//        //------------------------------------------------------
//        // 描画
//        //------------------------------------------------------
//        gpu::ps::setShader(shaderPs_Flat_);   // PS ピクセルシェーダー(テクスチャなし)
//
//        // 白色
//        gpu::setConstantBuffer("ColorCB", ColorCB{ float4(1.0f, 1.0f, 1.0f, 1.0f) });
//
//        // ライン線分描画
//        gpu::drawUserPrimitive(gpu::Primitive::LineList, static_cast<u32>(std::size(v)), v);
//    }
//}
//
////---------------------------------------------------------------------------
//// シーンの生成
////! @note テンプレートの特殊化で実装します
////---------------------------------------------------------------------------
//template<>
//std::unique_ptr<IScene> createScene<SceneDraw>()
//{
//    return std::make_unique<SceneDraw>();
//}
//}   // namespace scene