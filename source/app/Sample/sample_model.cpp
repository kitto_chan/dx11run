﻿////---------------------------------------------------------------------------
////!	@file	sample_model.cpp
////!	@brief	モデル描画サンプル
////---------------------------------------------------------------------------
//#include "sample_model.h"
//
//#include "renderer/model.h"
//namespace scene {
//
//namespace {
//
////--------------------------------------------------------------
//// 内部変数
////--------------------------------------------------------------
//
//std::shared_ptr<Model>     modelMiku_;   //!< 3Dモデル(初音ミク)
//std::shared_ptr<Model>     model_;       //!< 3Dモデル
//std::shared_ptr<Animation> animation_;   //!< 3Dアニメーション
//
//std::shared_ptr<Model>     modelMonster_;       //!< 3Dモデル
//std::shared_ptr<Animation> animationMonster_;   //!< 3Dアニメーション
//
//CameraDebug camera_;   //!< デバッグカメラ
//
//bool isDrawDebug_ = false;   //!< デバッグ描画をするかどうか
//
//// テクスチャ(Shader Resource)
//std::shared_ptr<gpu::Texture> texture1_;   //!< テクスチャ1
//
////--------------------------------------------------------------
//// シーン用定数バッファ
//// 定数バッファ構造体はシェーダー側と同じデーター構造にする必要あり
////--------------------------------------------------------------
//struct CameraCB
//{
//    matrix matView_ = math::identity();   //!< ビュー行列
//    matrix matProj_ = math::identity();   //!< 投影行列
//};
//
//CameraCB cameraCb_{};   //!< カメラ用定数バッファ
//
//}   // namespace
//
////--------------------------------------------------------------------------
////! 初期化
////--------------------------------------------------------------------------
//bool SceneModel::Init()
//{
//    //-------------------------------------------------------------
//    // アニメーションを読み込み
//    //-------------------------------------------------------------
//    {
//        constexpr Animation::Desc desc[] = {
//            // 名前,  アニメーションファイル名
//            { "push", "test_model3/Pushing.fbx" },
//            { "walk", "test_model3/Walking.fbx" },
//            { "dance", "test_model3/Brooklyn Uprock.fbx" },
//        };
//        if(!animation_) {
//            animation_ = createAnimation(desc, std::size(desc));
//        }
//        if(!animation_)
//            return false;
//
//        // 歩きアニメーションを再生
//        animation_->play("walk", Animation::PlayType::Loop);
//    }
//    {
//        constexpr Animation::Desc desc[] = {
//            // 名前,  アニメーションファイル名
//            { "dance", "test_model1/Chicken Dance.fbx" },
//        };
//        if(!animationMonster_) {
//            animationMonster_ = createAnimation(desc, std::size(desc));
//        }
//        if(!animationMonster_)
//            return false;
//
//        // 歩きアニメーションを再生
//        animationMonster_->play("dance", Animation::PlayType::Loop);
//    }
//    //-------------------------------------------------------------
//    // モデルの読み込み
//    //-------------------------------------------------------------
//    if(!model_) {
//        model_ = createModel("test_model3/Walking.fbx");
//    }
//    model_->bindAnimation(animation_);   // アニメーションをモデルに関連付け
//
//    //--------
//    // モンスター
//    if(!modelMonster_) {
//        modelMonster_ = createModel("test_model1/Chicken Dance.fbx");
//    }
//    modelMonster_->bindAnimation(animationMonster_);   // アニメーションをモデルに関連付け
//
//    //--------
//    // 初音ミク
//    if(!modelMiku_) {
//        modelMiku_ = createModel("appearance_miku/miku.fbx");
//    }
//
//    //----------------------------------------------------------
//    // テクスチャの読み込み
//    //----------------------------------------------------------
//    texture1_ = gpu::createTextureFromFile("dx11/sample.dds");
//
//    //----------------------------------------------------------
//    // カメラの初期位置設定
//    //----------------------------------------------------------
//    camera_.setPosition(float3(-1.0f, 1.0f, 3.0f));   // 位置座標
//    camera_.setLookAt(float3(-1.0f, 1.0f, 0.0f));     // 注視点
//
//    return true;
//}
//
////--------------------------------------------------------------------------
////! 更新
////! @attention ここでは描画発行は行わないこと
////--------------------------------------------------------------------------
//void SceneModel::Update(f32 deltaTime)
//{
//    // カメラ行列
//    camera_.update(deltaTime);
//    cameraCb_.matView_ = camera_.view();
//    cameraCb_.matProj_ = camera_.projection();
//
//    //==============================================================
//    // 3Dモデルのアニメーションを更新
//    //==============================================================
//    animation_->update(deltaTime);   // アニメーション再生はモデルのupdate()よりも先に実行
//    model_->update();
//
//    animationMonster_->update(deltaTime);
//    modelMonster_->update();
//
//    modelMiku_->update();
//
//    //----------------------------------------------------------
//    // ImGui ウィンドウ表示テスト
//    //----------------------------------------------------------
//    ImGui::SetNextWindowSize(ImVec2(360, 220), ImGuiCond_Once);
//    ImGui::Begin(u8"ウィンドウ表示テスト");
//    {
//        ImGui::Text(u8"【モデル描画・アニメーション再生サンプル】");
//        ImGui::Separator();
//        ImGui::Text(u8"モデル・アニメーション素材: https://www.mixamo.com/");
//        ImGui::Image(static_cast<ID3D11ShaderResourceView*>(*texture1_), ImVec2(128, 128));
//        ImGui::SameLine();
//        ImGui::Checkbox(u8"モデルのデバッグ描画", &isDrawDebug_);
//        ImGui::Text(u8"日本語表示 Japanese Language");
//    }
//    ImGui::End();
//}
//
////--------------------------------------------------------------------------
////! 描画
////! @attention ここでは移動更新は行わないこと
////--------------------------------------------------------------------------
//void SceneModel::Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
//{
//    //----------------------------------------------------------
//    // 描画バッファ設定
//    //----------------------------------------------------------
//    gpu::setRenderTarget(0, colorTexture);
//    gpu::setDepthStencil(depthTexture);
//
//    // カラーとデプスバッファをクリア
//    gpu::clearColor(colorTexture, float4(0.5f, 0.5f, 0.8f, 1.0f));   // カラーバッファ
//    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ
//
//    // 表裏カリングOFF
//    gpu::setRasterizerState(dx11::commonStates().CullNone());
//
//    // カメラ定数バッファ
//    gpu::setConstantBuffer("CameraCB", cameraCb_);
//
//    //==============================================================
//    // 軸の描画
//    //==============================================================
//    {
//        constexpr f32 gridSize = 64.0f;   // グリッドの大きさ
//
//        // 軸の描画
//        debug::drawLineF(float3(-gridSize, 0.0f, 0.0f), float3(+gridSize, 0.0f, 0.0f), Color(255, 0, 0));   // X軸
//        debug::drawLineF(float3(0.0f, -gridSize, 0.0f), float3(0.0f, +gridSize, 0.0f), Color(0, 255, 0));   // Y軸
//        debug::drawLineF(float3(0.0f, 0.0f, -gridSize), float3(0.0f, 0.0f, +gridSize), Color(0, 0, 255));   // Z軸
//    }
//
//    //==============================================================
//    // モデルを描画
//    //==============================================================
//    {
//        auto matWorld = math::translate(float3(-2.0f, 0.0f, 0.0f));
//
//        model_->setWorldMatrix(matWorld);
//        model_->render();   // 描画
//        if(isDrawDebug_) {
//            model_->renderDebug();   // デバッグ描画
//        }
//    }
//
//    //--------
//    // モンスターを描画
//    {
//        auto matWorld = math::translate(float3(0.0f, 0.0f, 0.0f));
//
//        modelMonster_->setWorldMatrix(matWorld);
//        modelMonster_->render();   // 描画
//        if(isDrawDebug_) {
//            modelMonster_->renderDebug();   // デバッグ描画
//        }
//    }
//
//    //--------
//    // 初音ミクを描画
//    {
//        // 配置
//        auto matWorld = math::translate(float3(-1.0f, 0.0f, +1.0f));
//
//        modelMiku_->setWorldMatrix(matWorld);
//        modelMiku_->render();   // 描画
//        if(isDrawDebug_) {
//            modelMiku_->renderDebug();   // デバッグ描画
//        }
//    }
//}
//
////---------------------------------------------------------------------------
//// シーンの生成
////! @note テンプレートの特殊化で実装します
////---------------------------------------------------------------------------
//template<>
//std::unique_ptr<IScene> createScene<SceneModel>()
//{
//    return std::make_unique<SceneModel>();
//}
//}   // namespace scene