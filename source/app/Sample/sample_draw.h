﻿////---------------------------------------------------------------------------
////!	@file	sample_draw.h
////!	@brief	描画サンプル
////---------------------------------------------------------------------------
//#pragma once
//namespace scene {
//
////===========================================================================
////! 描画サンプル用シーン
////===========================================================================
//class SceneDraw final : public IScene
//{
//public:
//    //! コンストラクタ
//    SceneDraw() = default;
//
//    //! デストラクタ
//    virtual ~SceneDraw() = default;
//
//    //! 初期化
//    virtual bool Init() override;
//
//    //! 更新
//    //! @param  [in]    deltaTime   進行時間(単位:秒)
//    virtual void Update(f32 deltaTime) override;
//
//    //! 描画
//    virtual void Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;
//};
//}   // namespace scene