﻿//---------------------------------------------------------------------------
//!	@file	TitleScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------

#include "TitleScene.h"
#include "manager/SceneManager.h"
namespace scene {

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
TitleScene::TitleScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
TitleScene::~TitleScene()
{
    // TODO: リファクタリング
    // 文字クリア
    render::FontRendererIns()->Clear();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool TitleScene::OnInit()
{
    auto* backBuffer  = gpu::swapChain()->backBuffer();
    f32   centerWidth = static_cast<f32>(backBuffer->desc().width_) / 2.0f;   //　画面中心点を求める

    // タイトル文字描画設定
    {
        std::wstring     title = L"RUN!!";
        render::FontDesc fontDesc(title.c_str(), float2(centerWidth, 200), WHITE, 4.0f);
        render::FontRendererIns()->SetFont("Title", fontDesc);
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void TitleScene::OnUpdate()
{
    // Push Push Space Key の文字　アニメション
    {
        auto*            backBuffer = gpu::swapChain()->backBuffer();
        f32              width      = static_cast<f32>(backBuffer->desc().width_) / 2.0f;
        std::wstring     push       = L"Push Space Key To Game Start";
        f32              scale      = 0.4f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;
        render::FontDesc fontDesc(push.c_str(), float2(width, 600), WHITE, scale);
        render::FontRendererIns()->SetFont("Push", fontDesc);
    }

    // スペースキーを押したら、ゲームシーンに切り替えます
    if(input::KeyboardMgr()->IsKeyPress(DirectX::Keyboard::Space)) {
        manager::SetNextScene();
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void TitleScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // ビューポート設定
    {
        auto*                   backBuffer = gpu::swapChain()->backBuffer();
        auto*                   context    = gpu::context();
        ID3D11RenderTargetView* rtv        = *colorTexture;
        context->OMSetRenderTargets(1, &rtv, nullptr);

        D3D11_VIEWPORT viewport;
        viewport.TopLeftX = 0;
        viewport.TopLeftY = 0;
        viewport.Width    = static_cast<f32>(backBuffer->desc().width_);
        viewport.Height   = static_cast<f32>(backBuffer->desc().height_);
        viewport.MinDepth = 0.0f;
        viewport.MaxDepth = 1.0f;
        context->RSSetViewports(1, &viewport);
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void TitleScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void TitleScene::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<TitleScene>()
{
    return std::make_unique<TitleScene>();
}

}   // namespace scene
