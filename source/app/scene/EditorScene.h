﻿//---------------------------------------------------------------------------
//!	@file	EditorScene.h
//!	@brief	制作のテストシーン
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"
namespace entity {
class Entity;
}

namespace manager {
class EntityManager;
}

namespace scene {
class EditorScene final : public BaseScene
{
public:
    EditorScene();    //! コンストラクター
    ~EditorScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放
};
}   // namespace scene
