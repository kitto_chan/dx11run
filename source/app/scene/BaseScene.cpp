﻿//---------------------------------------------------------------------------
//!	@file	BaseScene.cpp
//!	@brief	ゲームマネージャー
//---------------------------------------------------------------------------
#include "BaseScene.h"
#include "Manager/EntityManager.h"
#include "Entity/Entity.h"

namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
BaseScene::BaseScene()
{
    _pEntityMgr = std::make_unique<manager::EntityManager>();
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
BaseScene::~BaseScene()
{
    Finalize();
    _pEntityMgr.reset();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool BaseScene::Init()
{
    // 文字レンダー初期化
    render::FontRendererIns()->Init();

    // 各シーンの初期化
    OnInit();

    // エンティティ　初期化
    if(!_pEntityMgr->Init()) {
        return false;
    };

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void BaseScene::Update([[maybe_unused]] f32 deltaTime)
{
    if(input::KeyboardMgr()->IsKeyPress(DirectX::Keyboard::F1)) {
        SystemSettingsMgr()->SwapMode();
    }

    _pEntityMgr->Update();

    OnUpdate();
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void BaseScene::Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    OnRender(colorTexture, depthTexture);

    _pEntityMgr->Render();

    render::FontRendererIns()->Render();
}

//---------------------------------------------------------------------------
//! Imguiの描画
//---------------------------------------------------------------------------
void BaseScene::RenderImgui(raw_ptr<gpu::Texture> colorTexture)
{
    OnRenderImgui();

    _pEntityMgr->RenderImgui();

    if(SystemSettingsMgr()->IsEditorMode()) {
        ImGui::Begin("Scene");
        ImVec2                    contentSize = ImGui::GetContentRegionAvail();   // ウインドウズの"中身"のサイズ
        ID3D11ShaderResourceView* srv         = *colorTexture;                    // テクスチャへのレンダー（Render-To-Texture）
        ImGui::Image((void*)srv, contentSize);                                    // 今画面のテクスチャーImgui DockSpaceに設置
        ImGui::End();
    }
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void BaseScene::Finalize()
{
    //OnFinalize();

    _pEntityMgr->Finalize();
}
//---------------------------------------------------------------------------
//! entity managerを取得
//---------------------------------------------------------------------------
raw_ptr<manager::EntityManager> BaseScene::GetEntityMgr()
{
    return _pEntityMgr.get();
}
}   // namespace scene
