﻿//---------------------------------------------------------------------------
//!	@file	EditorScene.h
//!	@brief	制作のテストシーン
//---------------------------------------------------------------------------
#include <iomanip>

// テスト用　こういうふうにインクルードはデザインとして悪いと思う
#include "EditorScene.h"
#include "imgui/imgui.h"
#include "manager/EntityManager.h"
#include "manager/CubeManager.h"
#include "manager/SceneManager.h"

#include "effect/Effect.h"
#include "effect/skyBox/StaticSkyBoxEffect.h"
#include "entity/skybox/StaticSkyBox.h"

#include "entity/gameObject/Camera/ThirdPersonCamera.h"
#include "entity/gameObject/Camera/FirstPersonCamera.h"
#include "entity/gameObject/Character/Player.h"

#include "component/Transform.h"

namespace scene {
namespace {

s32 _scoreCount = 0;   // TODO: better handle

//! カメラ用定数バッファ
struct CameraCB
{
    matrix matView = math::identity();   //!< ビュー行列
    matrix matProj = math::identity();   //!< 投影行列
};

CameraCB _cameraCB{};   //!< カメラ用定数バッファ

// DEBUG 用
// TODO: remove
gameobject::ThirdPersonCamera* TPCamera;
gameobject::Player*            player;

uni_ptr<entity::StaticSkyBox> _pCubeMap;

uni_ptr<manager::CubeManager> _cubeMgr;

}   // namespace

//==========================================================================
// EditorScene
//==========================================================================

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
EditorScene::EditorScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
EditorScene::~EditorScene()
{
    // TODO: リファクタリング
    // 文字クリア
    render::FontRendererIns()->Clear();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool EditorScene::OnInit()
{
    // エフェクトの初期化
    effect::BasicEffectIns()->InitAll();
    effect::StaticSkyBoxEffectIns()->InitAll();

    TPCamera = new gameobject::ThirdPersonCamera();
    player   = new gameobject::Player();
    _pEntityMgr->Regist(player);
    _pEntityMgr->Regist(TPCamera);

    _cubeMgr = std::make_unique<manager::CubeManager>();
    _cubeMgr->Init(_pEntityMgr.get());

    _pCubeMap = std::make_unique<entity::StaticSkyBox>();
    _pCubeMap->ReadFile("gameSource/skyBox/Space.dds");
    _pCubeMap->Init();

    TPCamera->GetComponent<component::Transform>()->SetRotation(math::D2R(-40.f), math::D2R(-135.f), 0.0f);
    TPCamera->SetTarget(player);
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void EditorScene::OnUpdate()
{
    auto* backBuffer   = gpu::swapChain()->backBuffer();
    f32   screenWidth  = static_cast<f32>(backBuffer->desc().width_);
    f32   screenHeight = static_cast<f32>(backBuffer->desc().height_);

    if(player->IsDead()) {
        render::FontRendererIns()->RemoveFont("Score");

        {
            std::wstring     gameOverMsg = L"GameOver";
            render::FontDesc overFont    = render::FontDesc(gameOverMsg.c_str(),
                                                         float2(screenWidth * 0.5f, screenHeight * 0.3f),
                                                         float4(1.0f, 0.0f, 0.0f, 1.0f),
                                                         2.0f);
            render::FontRendererIns()->SetFont("GameOver", overFont);
        }

        {
            std::wstringstream wss;
            wss << "Score: " << _scoreCount;
            std::wstring     gameOverMsg = L"LastScore";
            render::FontDesc overFont    = render::FontDesc(wss.str(),
                                                         float2(screenWidth * 0.5f, screenHeight * 0.5f),
                                                         float4(1.0f, 1.0f, 1.0f, 1.0f),
                                                         1.0f);
            render::FontRendererIns()->SetFont("LastScore", overFont);
        }

        // Push Space Key To Play Again の文字　アニメション
        {
            std::wstring     push  = L"Push Space Key To Play Again";
            f32              scale = 0.3f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;
            render::FontDesc fontDesc(push.c_str(), float2(screenWidth * 0.5, screenHeight * 0.7f), WHITE, scale);
            render::FontRendererIns()->SetFont("Push", fontDesc);
        }

        if(input::KeyboardMgr()->IsKeyPress(DirectX::Keyboard::Space)) {
            render::FontRendererIns()->Clear();
            _scoreCount = 0;
            manager::SetNextScene();
        }

        return;
    }

    // 画面の右上スコア描画
    {
        f32 drawXPos = screenWidth * 0.9f;   //　画面中心点を求める
        s32 level    = static_cast<s32>(manager::GameMgr()->GetCurrentLevel());
        _scoreCount += level + 1;
        std::wstringstream wss;
        wss << "Score: " << std::setw(5) << _scoreCount;
        render::FontDesc fontDesc = render::FontDesc(wss.str(), float2(drawXPos, 50), WHITE, 0.7f);
        render::FontRendererIns()->SetFont("Score", fontDesc);
    }

    // 表裏カリングOFF
    gpu::setRasterizerState(gpu::commonStates().CullNone());

	// カメラバッファ更新
    _cameraCB.matView = TPCamera->GetViewMatrix();
    _cameraCB.matProj = TPCamera->GetProjMatrix();

    // キューブマネージャー
    float3 playerPos = player->GetComponent<component::Transform>()->GetPosition();
    _cubeMgr->Update(_pEntityMgr.get(), playerPos.z);

    // スカイボックスアップデート
    _pCubeMap->Update(_cameraCB.matView, _cameraCB.matProj);
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void EditorScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    gpu::setConstantBuffer("CameraCB", _cameraCB);

    effect::BasicEffectIns()->SetEyePos(player->GetComponent<component::Transform>()->GetPosition());

    _pCubeMap->Render();

    //physics::PhysicsEngine::instance()->renderDebug();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void EditorScene::OnRenderImgui()
{
    ui::ImguiTopBar();

    if(_pEntityMgr->GetSelectedEntities()) {
        ui::ImguiGuizmoView(TPCamera->GetViewMatrix(),
                            TPCamera->GetProjMatrix(),
                            _pEntityMgr->GetSelectedEntities()->GetComponent<component::Transform>());
    }
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void EditorScene::OnFinalize()
{
    _cubeMgr->Finalize();
}

//---------------------------------------------------------------------------
// シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<EditorScene>()
{
    return std::make_unique<EditorScene>();
}

}   // namespace scene
