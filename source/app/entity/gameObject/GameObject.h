﻿//---------------------------------------------------------------------------
//!	@file	GameObject.h
//!	@brief	ゲームオブジェクトの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "../Entity.h"
namespace component {
class Transform;
};

namespace entity::go {
class GameObject : public Entity
{
public:
    GameObject();
    GameObject(const std::string& name);
    GameObject(const float3& pos);
    GameObject(const std::string& name, const float3& pos);
    virtual ~GameObject();

    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------
    
protected:
    //---------------------------------------------------------------------------
    //! public 変数
    //---------------------------------------------------------------------------
    
    raw_ptr<component::Transform> _pTransform; //!< オブジェクトの位置、回転、スケールを扱うクラス
private:
    //---------------------------------------------------------------------------
    //! private 関数
    //---------------------------------------------------------------------------
	// 継承用
    //bool OnInit()        override;  //!< 初期化
	//void OnRender()      override;  //!< 描画
    //void OnUpdate()      override;  //!< 更新
    void OnRenderImgui() override;  //!< ImGui描画
    //void OnFinalize()    override;  //!< 解放

};
}   // namespace entity::go
