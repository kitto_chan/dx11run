﻿//---------------------------------------------------------------------------
//!	@file	BaseCamera.h
//!	@brief	カメラの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "../GameObject.h"
namespace component {
class Transform;
};

namespace entity::go {
class BaseCamera : public GameObject
{
public:
    BaseCamera();
    BaseCamera(const std::string& name);
    virtual ~BaseCamera();

    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------

    // 行列
    matrix GetLocalToWorldMatrix() const;   //!< ローカルからワールド変換
    matrix GetViewMatrix() const;           //!< ビュー行列
    matrix GetProjMatrix() const;           //!< 投影行列

    void SetTarget(raw_ptr<GameObject> pTarget);   //!< カメラの対象を取得
protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

    f32 _fovY   = 35.0f * (math::PI / 180.0f);   //!< 画角
    f32 _aspect = 16.0f / 9.0f;                  //!< アスペクト
    f32 _nearZ  = 0.01f;                         //!< 最近距離
    f32 _farZ   = 1000.0f;                       //!< 最遠距離

    raw_ptr<GameObject> _pTarget;   //!< カメラの対象

    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------
    virtual void OnUpdate() override;   //!< 更新

    //! カメラ対象のtransformを取得
    raw_ptr<component::Transform> GetTargetTransform() const;
};
}   // namespace entity::go
