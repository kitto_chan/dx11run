﻿//---------------------------------------------------------------------------
//!	@file	GameObject.h
//!	@brief	ゲームオブジェクトの基底クラス
//---------------------------------------------------------------------------
#include "GameObject.h"
#include "Component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameObject::GameObject()
{
    _pTransform = AddComponent<component::Transform>();
    _name       = "GameObject";
    //_tags |= GameTag_GameObject;
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
GameObject::GameObject(const std::string& name)
: Entity(name)
{
    _pTransform = AddComponent<component::Transform>();
    //_tags |= GameTag_GameObject;
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] pos ゲームオブジェクトの初期座標
//---------------------------------------------------------------------------
GameObject::GameObject(const float3& pos)
{
    _pTransform = AddComponent<component::Transform>();
    _pTransform->SetPosition(pos);

	_name       = "GameObject";
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//! [in] pos ゲームオブジェクトの初期座標
//---------------------------------------------------------------------------
GameObject::GameObject(const std::string& name, const float3& pos)
: Entity(name)
{
    _pTransform = AddComponent<component::Transform>();
    _pTransform->SetPosition(pos);
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameObject::~GameObject()
{
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GameObject::OnRenderImgui()
{

}
}   // namespace entity::go
