﻿//---------------------------------------------------------------------------
//!	@file	scene.cpp
//!	@brief	シーン管理
//---------------------------------------------------------------------------
#include "../manager/SceneManager.h"

namespace scene {
class EditorScene;
class TitleScene;
}   // namespace scene

extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::EditorScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::TitleScene>();

namespace manager {

namespace {

std::unique_ptr<scene::BaseScene> scene_;       //!<　今のシーン
std::unique_ptr<scene::BaseScene> sceneNext_;   //!<　次のシーン

}   // namespace

//--------------------------------------------------------------------------
// 初期化
//--------------------------------------------------------------------------
bool onInitialize([[maybe_unused]] u32 width, [[maybe_unused]] u32 height)
{
    //----------------------------------------------------------
    // 初期シーンの作成
    //----------------------------------------------------------
    sceneNext_ = scene::createNewScene<scene::TitleScene>();
    if(!sceneNext_)
        return false;

    return true;
}

//--------------------------------------------------------------------------
//! 更新
//! @attention ここでは描画発行は行わないこと
//--------------------------------------------------------------------------
void onUpdate(f32 deltaTime)
{
    if(sceneNext_) {
        scene_ = std::move(sceneNext_);

        // 初期化
        if(!scene_->Init()) {
            scene_.reset();   // 解放
        }
    }

    //----------------------------------------------------------
    // 更新実行
    //----------------------------------------------------------
    if(scene_) {
        scene_->Update(deltaTime);
    }
}

//--------------------------------------------------------------------------
//! 描画
//! @attention ここでは移動更新は行わないこと
//--------------------------------------------------------------------------
void onRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    if(scene_) {
        scene_->Render(colorTexture, depthTexture);
        if(SystemSettingsMgr()->IsEditorMode()) {
            scene_->RenderImgui(colorTexture);
        }
    }
}

//--------------------------------------------------------------------------
//! 解放
//--------------------------------------------------------------------------
void onFinalize()
{
    scene_.reset();
    sceneNext_.reset();
}
//--------------------------------------------------------------------------
//!　シーンを切り替える
//--------------------------------------------------------------------------
void SetNextScene()
{
    sceneNext_ = scene::createNewScene<scene::EditorScene>();
}
}   // namespace manager
