﻿//---------------------------------------------------------------------------
//!	@file	CameraManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#pragma once

namespace entity {
namespace go {
class BaseCamera;
}   // namespace go
}   // namespace entity
namespace gameobject = entity::go;


namespace manager {

class CameraManager : public Singleton<CameraManager>
{
public:
    CameraManager();
    ~CameraManager();

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    bool Init();       //!< 初期化
    void Update();     //!< 更新
    void Render();     //!< 描画
    void Finalize();   //!< 解放

    raw_ptr<gameobject::BaseCamera> GetCurrentCamera() const;

private:
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------
    std::vector<uni_ptr<gameobject::BaseCamera>> _cameras;

    raw_ptr<gameobject::BaseCamera> _pCurrentCamera;

    //! 追加カメラ
    void AddCamera(uni_ptr<gameobject::BaseCamera> newCamera, [[maybe_unused]] bool setToCurrent = false);
};
//! カメラ管理クラスを取得
manager::CameraManager* CameraMgr();

}   // namespace manager
