﻿//---------------------------------------------------------------------------
//!	@file	CameraManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#pragma once
#include "CameraManager.h"
#include "entity/gameObject/Camera/FirstPersonCamera.h"
#include "entity/gameObject/Camera/ThirdPersonCamera.h"
#include "component/Transform.h"
namespace manager {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
CameraManager::CameraManager()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
CameraManager::~CameraManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CameraManager::Init()
{
    // デフォルトカメラ
	uni_ptr<gameobject::ThirdPersonCamera> defaultCamera = std::make_unique<gameobject::ThirdPersonCamera>();
    _cameras.push_back(std::move(defaultCamera));
    _pCurrentCamera = defaultCamera.get();

    /*effect::BasicEffectIns()->SetWorldMatrix(DirectX::XMMatrixIdentity());
    effect::SkinnedMeshEffectIns()->SetWorldMatrix(DirectX::XMMatrixIdentity());
    effect::BasicPosColEffectIns()->SetWorldMatrix(DirectX::XMMatrixIdentity());*/
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void CameraManager::Update()
{
    //effect::BasicEffectIns()->SetViewMatrix(_pCurrentCamera->GetViewMatrix());
    //effect::BasicEffectIns()->SetEyePos(_pCurrentCamera->GetPosition());
    //effect::BasicEffectIns()->SetProjMatrix(_pCurrentCamera->GetProjXM());

    //effect::BasicPosColEffectIns()->SetViewMatrix(_pCurrentCamera->GetViewXM());
    //effect::BasicPosColEffectIns()->SetProjMatrix(_pCurrentCamera->GetProjXM());

    //effect::SkinnedMeshEffectIns()->SetViewMatrix(_pCurrentCamera->GetViewXM());
    //effect::SkinnedMeshEffectIns()->SetEyePos(_pCurrentCamera->GetPosition());
    //effect::SkinnedMeshEffectIns()->SetProjMatrix(_pCurrentCamera->GetProjXM());
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void CameraManager::Render()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void CameraManager::Finalize()
{
}

raw_ptr<gameobject::BaseCamera> CameraManager::GetCurrentCamera() const
{
    return _pCurrentCamera;
}

void CameraManager::AddCamera(uni_ptr<gameobject::BaseCamera> newCamera, [[maybe_unused]] bool setToCurrent)
{
}

//---------------------------------------------------------------------------
//! カメラ管理クラスを取得
//---------------------------------------------------------------------------
manager::CameraManager* CameraMgr()
{
    return CameraManager::Instance();
}
}   // namespace manager
