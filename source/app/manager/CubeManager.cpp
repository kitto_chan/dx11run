﻿//---------------------------------------------------------------------------
//!	@file	CameraManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#pragma once
#include "CubeManager.h"
#include "manager/EntityManager.h"
#include "entity/gameObject/Cube.h"
#include "component/Transform.h"
#include "component/action/CubeAction.h"
#include "component/action/FloorAction.h"
#include "component/action/RollingPinAction.h"

namespace manager {
namespace {
constexpr s32 FLOOR_LENGTH = 30;     //!< キャラ前方生成のキューブ数
constexpr f32 SIDE_LENGTH  = 1.0f;   //!< キューブの長さ

constexpr s32 ZInitSize = 15;   // 床としてZ軸初期生成数
constexpr s32 XInitSize = 5;    // 床としてX軸生成数(+-値)

f32 _lastTime = 0.0f;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
CubeManager::CubeManager()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
CubeManager::~CubeManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CubeManager::Init(raw_ptr<EntityManager> enttMgr)
{
    float3 cubeSize = { SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH };   // キューブサイズ

    for(s32 z = 0; z < ZInitSize; z++) {
        f32 fallTime = 20.0f * (_endPointPos + 1.0f);

        for(s32 x = -XInitSize; x < XInitSize; x++) {
            {
                gameobject::Cube* cube;
                cube = new gameobject::Cube("Floor", float3(x, 0.f, z));
                cube->AddComponent<component::FloorAction>(fallTime);
                enttMgr->Regist(cube);
            }
        }

        _endPointPos += SIDE_LENGTH;
    }
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void CubeManager::Update(raw_ptr<EntityManager> enttMgr, f32 playerPosZ)
{
    _count++;

    //　レベル計算
    {
        s32 level = static_cast<s32>(playerPosZ / 50);
        level     = std::min(level, static_cast<s32>(GameLevel::Level_MAX) - 1);
        manager::GameMgr()->SetCurrentLevel(GameLevel(level));
    }

    // 床生成
    CheckFloor(enttMgr, playerPosZ);

    // 障害物生成
    {
        constexpr s32 spawnDist = 40;   // プレイヤー先40の距離から生成

        s32 level        = static_cast<s32>(manager::GameMgr()->GetCurrentLevel());
        s32 spawnPerTime = 80 - level * 10;
        s32 spawnNum     = 2 + level;

        if(_count % spawnPerTime == 0) {
            RandInitCube(enttMgr, spawnNum, playerPosZ + spawnDist);
        }
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void CubeManager::Render()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void CubeManager::Finalize()
{
}
//---------------------------------------------------------------------------
//! ランダムでキューブを生成
//---------------------------------------------------------------------------
void CubeManager::RandInitCube(raw_ptr<EntityManager> enttMgr, s32 num, f32 playerPosZ)
{
    using namespace gameobject;

    const float3 initCubeSize = float3(1.f, 2.f, 1.f);
    for(s32 i = 0; i < num; i++) {
        f32 posX = math::GetRandomF(-4.0f, 5.0f);

        const float3 initPos = float3(posX, 1.1f, playerPosZ + 50);

        Cube* newCube = new Cube("BlockCube",
                                 initPos,
                                 initCubeSize,
                                 true);
        newCube->AddComponent<component::CubeAction>();
        enttMgr->Regist(newCube);
    }
}
//---------------------------------------------------------------------------
//! 床生成
//---------------------------------------------------------------------------
void CubeManager::CheckFloor(raw_ptr<EntityManager> enttMgr, f32 playerPosZ)
{
    // 着いたた最遠距離
    _playerMaxDis = std::max(playerPosZ, _playerMaxDis);

    s32 level    = static_cast<s32>(manager::GameMgr()->GetCurrentLevel());
    f32 time     = 20.0f;
    f32 fallTime = time * (_endPointPos + 1.0f) - _count;
    if(_playerMaxDis + FLOOR_LENGTH > _endPointPos) {
        for(s32 x = -XInitSize; x < XInitSize; x++) {
            // レベルによって床が生成しなくなります
            switch(manager::GameMgr()->GetCurrentLevel()) {
                case GameLevel::Level_1:
                    // なんもしない
                    break;
                case GameLevel::Level_2:
                    if(math::GetRandomI(10) > 8)
                        continue;
                    break;
                case GameLevel::Level_3:
                    if(math::GetRandomI(10) > 7)
                        continue;
                    break;
                case GameLevel::Level_4:
                    if(math::GetRandomI(10) > 5)
                        continue;
                    break;
            }

            gameobject::Cube* cube;
            cube = new gameobject::Cube("Floor", float3(x, 0.f, _endPointPos));
            cube->AddComponent<component::FloorAction>(fallTime);
            enttMgr->Regist(cube);
        }

        // Rolling Cubeを生成
        {
            s32 initPerTime = 55 - level * 10;

            f32 size_z = 6.0f + level * 3;
            if((s32)_endPointPos % initPerTime == 0) {
                gameobject::Cube* cube;
                cube = new gameobject::Cube("Rolling", float3(-0.5f, 1.f, _endPointPos), float3(0.5f, 1.f, size_z), true);
                cube->AddComponent<component::RollingPinAction>(fallTime);
                enttMgr->Regist(cube);
            }
        }
        _endPointPos += SIDE_LENGTH;
    }
}
}   // namespace manager
