﻿//---------------------------------------------------------------------------
//!	@file	EntityManager.h
//!	@brief	エンティティマネージャー
//---------------------------------------------------------------------------
#pragma once
namespace entity {
class Entity;
}
using namespace entity;

namespace manager {
class EntityManager
{
public:
    EntityManager();
    ~EntityManager();

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------
    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void Render();        //!< 描画
    void RenderImgui();   //!< Imgui描画
    void Finalize();      //!< 解放

    void Regist(Entity* entity);     //!< エンティティを登録
    void Unregist(Entity* entity);   //!< エンティティの登録解除
    void UnregistAll();              //!< すべてエンティティの登録解除

    std::vector<Entity*> GetEntities() const;   //!< エンティティを取得

	raw_ptr<Entity> GetSelectedEntities() const;
private:
    //---------------------------------------------------------------------------
    //! 内部変数
    //---------------------------------------------------------------------------
    std::vector<Entity*> _entities;   //!< エンティティの配列

	u64 _selected = 0;
};
}   // namespace manager
