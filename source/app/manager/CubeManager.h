﻿//---------------------------------------------------------------------------
//!	@file	CubeManager.h
//!	@brief	ゲームキューブ管理
//---------------------------------------------------------------------------
#pragma once

namespace entity {
namespace go {
class Cube;
}
}   // namespace entity::go
namespace gameobject = entity::go;

namespace manager {
class EntityManager;

class CubeManager
{
public:
    CubeManager();    //<! コンストラクタ
    ~CubeManager();   //<! デストラクタ

    // コピー禁止/代入禁止
    CubeManager(const CubeManager&) = delete;
    CubeManager(CubeManager&&)      = delete;
    CubeManager& operator=(const CubeManager&) = delete;
    CubeManager& operator=(CubeManager&&) = delete;

public:
    //---------------------------------------------------------------------------
    //public 関数
    //---------------------------------------------------------------------------
    bool Init(raw_ptr<EntityManager> enttMgr);                     //!< 初期化
    void Update(raw_ptr<EntityManager> enttMgr, f32 playerPosZ);   //!< 更新
    void Render();                                                 //!< 描画
    void Finalize();                                               //!< 解放

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //! ランダムでキューブを生成
    void RandInitCube(raw_ptr<EntityManager> enttMgr, s32 num, f32 playerPosZ);
    //! 床生成
    void CheckFloor(raw_ptr<EntityManager> enttMgr, f32 playerPosZ);

    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------

    s32 _count = 0;   //!< カウント

    f32 _playerMaxDis = 0.0f;   //!< プレイヤーのが到達した最大距離
    f32 _endPointPos  = 0.0f;   //!< 最遠生成距離

	f32 _startPoint;
};

}   // namespace manager
