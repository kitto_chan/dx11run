﻿//---------------------------------------------------------------------------
//!	@file	EntityManager.cpp
//!	@brief	エンティティマネージャー
//---------------------------------------------------------------------------
#include "EntityManager.h"
#include "Entity/Entity.h"

namespace manager {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
EntityManager::EntityManager()
{
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
EntityManager::~EntityManager()
{
    UnregistAll();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool EntityManager::Init()
{
    for(auto& _entity : _entities) {
        if(!_entity->_isInited) {
            _entity->Init();
        }
    }

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void EntityManager::Update()
{
    /*   for(int i = (int)m_entities.size() - 1; i >= 0; --i) {
        auto _entity = m_entities[i];
        if(_entity->m_is_release) {
            Unregist(_entity);
            continue;
        }
    }*/

    //for(auto entity : _entities) {
    //    if(!entity->_isInited) {
    //        entity->Init();
    //    }

    //    entity->Update();
    //}

    for(auto& entity : _entities) {
        if(!entity->_isInited) {
            entity->Init();
        }

        if(entity->_isRelease) {
            Unregist(entity);
        }

        entity->Update();
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void EntityManager::Render()
{
    for(auto entity : _entities) {
        entity->Render();
    }
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void EntityManager::RenderImgui()
{
    ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None;
    ImGui::Begin("Inspector");
    ImGui::SetWindowSize({ 200, sysst::WND_H });
    if(ImGui::BeginTabBar("Settings", tab_bar_flags)) {
        if(ImGui::BeginTabItem("GameObject")) {
            ImGui::PushItemWidth(-1);
            ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.2f, 0.2f, 0.2f, 1.0f));
            //GameObjectのリスト
            ImGui::Text("All GameObject");
            //static size_t selected = 0;
            if(ImGui::ListBoxHeader("", (int)_entities.size(), 10)) {
                for(size_t i = 0; i < _entities.size(); i++) {
                    //if((_entities[i]->_tags & GameTag_GameObject)) {
                    std::string itemId = _entities[i]->_name + "  " + std::to_string(i);
                    if(ImGui::Selectable(itemId.c_str(), _selected == i))
                        _selected = i;
                    //}
                }

                ImGui::ListBoxFooter();
            }
            ImGui::PopItemWidth();
            ImGui::PopStyleColor();
            ImGui::Separator();

            if(!_entities.empty()) {
                _entities[_selected]->RenderImgui();
            }
            ImGui::EndTabItem();

            if(ImGui::BeginTabItem("Entity")) {
                ImGui::Text("All Entity");

                if(ImGui::ListBoxHeader("", (int)_entities.size(), 10)) {
                    for(size_t i = 0; i < _entities.size(); i++) {
                        //if((_entities[i]->_tags & GameTag_Entity) && !(_entities[i]->_tags & GameTag_GameObject)) {
                        std::string itemId = _entities[i]->_name + "  " + std::to_string(i);
                        if(ImGui::Selectable(itemId.c_str(), _selected == i))
                            _selected = i;
                        //}
                    }

                    ImGui::ListBoxFooter();
                }
                ImGui::EndTabItem();
            }
            ImGui::EndTabBar();
        }
    }
    ImGui::End();
    //   /* for(auto entity : _entities) {
    //       entity->RenderImgui();
    //   }*/
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void EntityManager::Finalize()
{
    for(auto entity : _entities) {
        if(!entity->_isRelease)
            entity->Finalize();
    }
}

//---------------------------------------------------------------------------
//! エンティティを登録
//---------------------------------------------------------------------------
void EntityManager::Regist(Entity* entity)
{
    _entities.push_back(entity);
}
//---------------------------------------------------------------------------
//! 特定エンティティの登録を解除
//---------------------------------------------------------------------------
void EntityManager::Unregist(Entity* entity)
{
    for(auto itr = _entities.begin(); itr != _entities.end(); ++itr) {
        if(*itr == entity) {
            Entity* _entity = *itr;

            if(_entity->_isRelease) {
                _entity->Finalize();
                delete(_entity);
            }
            _entities.erase(itr);
            break;
        }
    }
}
//---------------------------------------------------------------------------
//! 既存のエンティティの登録を解除
//---------------------------------------------------------------------------
void EntityManager::UnregistAll()
{
    for(auto itr = _entities.rbegin(); itr != _entities.rend(); ++itr) {
        Entity* _entity = *itr;

        if(!_entity->_isRemoved) {
            _entity->_isRemoved = true;
            delete _entity;
        }
    }

    _entities.clear();
}
//---------------------------------------------------------------------------
//! エンティティのリストを取得
//---------------------------------------------------------------------------
std::vector<Entity*> EntityManager::GetEntities() const
{
    return _entities;
}
raw_ptr<Entity> EntityManager::GetSelectedEntities() const
{
    try {
        return _entities.at(_selected);
    }
    catch([[maybe_unused]] std::out_of_range& outOfRange) {
		// もしout of range nullptrを返却
        return nullptr;
	}
}
}   // namespace manager
