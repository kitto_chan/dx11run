﻿//---------------------------------------------------------------------------
//!	@file	CubeMapEffect.h
//!	@brief	スカイマップエフェクト
//---------------------------------------------------------------------------
#include "StaticSkyBoxEffect.h"

namespace effect {

class StaticSkyBoxEffectImpl final : public StaticSkyBoxEffect
{
public:
    struct CBVertexBuffer
    {
        matrix worldViewProj;
    };

public:
    StaticSkyBoxEffectImpl()  = default;
    ~StaticSkyBoxEffectImpl() = default;

    static StaticSkyBoxEffectImpl* Instance();

public:
    //---------------------------------------------------------------------------
    //! 定数バッファ
    //---------------------------------------------------------------------------
    CBuffer<0, CBVertexBuffer> _cbVertexBuffer;

    //---------------------------------------------------------------------------
    //! シェーダ
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Shader> _pVSSkyBox;   //!< 頂点シェーダ
    shr_ptr<gpu::Shader> _pPSSkyBox;   //!< ピクセルシェーダ

    //---------------------------------------------------------------------------
    //! 入力レイアウト
    //---------------------------------------------------------------------------
    shr_ptr<gpu::InputLayout> _pILVertexPos;

    //---------------------------------------------------------------------------
    //! テクスチャー
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Texture> _pTextureSkyBox;   //!< SRV

    //---------------------------------------------------------------------------
    //　変数
    //---------------------------------------------------------------------------
    bool _isDirty = false;
};

bool StaticSkyBoxEffect::InitAll()
{
    _pImpl              = StaticSkyBoxEffectImpl::Instance();
    _pImpl->_pVSSkyBox = gpu::createShader("gameSource/skyBox/StaticSkyBox_VS.hlsl", "VS", "vs_5_0");   // 頂点シェーダー
    if(!_pImpl->_pVSSkyBox) {
        return false;
    }
    _pImpl->_pILVertexPos = gpu::createInputLayout(vertex::VertexPos::inputLayout, std::size(vertex::VertexPos::inputLayout));

    //　シェーダー
    _pImpl->_pPSSkyBox = gpu::createShader("gameSource/skyBox/StaticSkyBox_PS.hlsl", "PS", "ps_5_0");   // 頂点シェーダー
    if(!_pImpl->_pPSSkyBox) {
        return false;
    }

    // 動的バッファを生成
    _pImpl->_cbVertexBuffer.CreateDynamicBuffer();

	// バインド
    _pImpl->_cbVertexBuffer.Bind("CBVertex");

    return true;
}
void StaticSkyBoxEffect::SetRenderDefault()
{
    gpu::setInputLayout(_pImpl->_pILVertexPos);
    gpu::vs::setShader(_pImpl->_pVSSkyBox);
    gpu::ps::setShader(_pImpl->_pPSSkyBox);
    gpu::ps::setSamplerState(0, render::RenderStatesIns()->_pSSLinearWrap);
    gpu::setRasterizerState(render::RenderStatesIns()->_pRSNoCull);
    gpu::setDepthStencilState(render::RenderStatesIns()->_pDSSLessEqual.Get());
    gpu::setBlendState(nullptr);
}
void StaticSkyBoxEffect::SetWorldViewProjMatrix(matrix world, matrix view, matrix proj)
{
    auto& cBuffer              = _pImpl->_cbVertexBuffer;
    cBuffer.data.worldViewProj = mul(mul(world, view), proj);
    _pImpl->_isDirty = cBuffer._isDirty = true;
}
void StaticSkyBoxEffect::SetWorldViewProjMatrix(matrix wvp)
{
    auto& cBuffer              = _pImpl->_cbVertexBuffer;
    cBuffer.data.worldViewProj = wvp;
    _pImpl->_isDirty = cBuffer._isDirty = true;
}
void StaticSkyBoxEffect::SetTextureCube(std::shared_ptr<dx11::Texture> textureCube)
{
    _pImpl->_pTextureSkyBox = textureCube;
}

void StaticSkyBoxEffect::Apply()
{
    gpu::ps::setTexture(0, _pImpl->_pTextureSkyBox);

    //!　更新
    if(_pImpl->_isDirty) {
        _pImpl->_isDirty = false;

        _pImpl->_cbVertexBuffer.SetnUpdateBuffer();
    }
}
//---------------------------------------------------------------------------
//! 実体を取得
//---------------------------------------------------------------------------
effect::StaticSkyBoxEffectImpl* StaticSkyBoxEffectImpl::Instance()
{
    static effect::StaticSkyBoxEffectImpl Instance;   // gpu::Renderのシングルトン実体
    return &Instance;
}

//------------------------------------------------------------------------
//! CubeMapEffect 管理クラスを取得
//------------------------------------------------------------------------
effect::StaticSkyBoxEffect* StaticSkyBoxEffect::Instance()
{
    return StaticSkyBoxEffectImpl::Instance();
}

effect::StaticSkyBoxEffect* StaticSkyBoxEffectIns()
{
    return StaticSkyBoxEffect::Instance();
}

}   // namespace effect
