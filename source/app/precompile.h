﻿//---------------------------------------------------------------------------
//!	@file	precompile.h
//!	@brief	プリコンパイルヘッダー
//---------------------------------------------------------------------------
#pragma once

#include "utility/GameDef.h" //!< ゲーム用の定義

#include "framework.h"

//! DirectX11実装をgpuとして名前をエイリアス
namespace gpu = dx11;

#include "manager/GameManager.h" //!< ゲームマネージャー
#include "sample/scene.h"   //!< シーン管理
