﻿//---------------------------------------------------------------------------
//! @file	InputControl.h
//!	@brief	プレイヤーの入力処理のコンポーネント
//---------------------------------------------------------------------------
#include "InputControl.h"
#include "Component/Transform.h"
#include "Entity/Entity.h"
namespace component {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
InputControl::InputControl()
{
    _name = "InputControl";
}
//---------------------------------------------------------------------------
//! デスストラクタ
//---------------------------------------------------------------------------
InputControl::~InputControl()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool InputControl::Init()
{
    _pTransform = _pOwner->GetComponent<Transform>();
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void InputControl::Update()
{
    MoveControl();
    //LookControl();
}

void InputControl::RenderImgui()
{
    ImGui::DragFloat("MoveSpeed", &_moveSpeed);
    ImGui::DragFloat("RotateSpeed", &_rotSpeed);
}

void InputControl::MoveControl()
{
    // 前進
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::W)) {
        MoveFB(_moveSpeed * -1);
    }
    // 後退
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::S)) {
        MoveFB(_moveSpeed);
    }
    // 左移動
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::A)) {
        //MoveLR(_moveSpeed );
        _pTransform->RotateYAxis(DirectX::XMConvertToRadians(_rotSpeed));
    }
    // 右移動
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::D)) {
        //MoveLR(_moveSpeed * -1);
        _pTransform->RotateYAxis(DirectX::XMConvertToRadians(_rotSpeed * -1));
    }
}

void InputControl::LookControl()
{
    if(!input::MouseMgr()->IsRelativeMode()) return;

    f32 dt = timer::TimerIns()->DeltaTime();

    // 回転
    f32 rotRadX   = input::MouseMgr()->GetPosY() * dt * _rotSpeed;
    f32 rotationX = _pTransform->GetRotation().x;
    rotationX += rotRadX;

    // "倒立"しないように
    // 回転X角度　は [PI * 7/ 18] の範囲で制限します
    f32 rotRange = DirectX::XM_PI * 7 / 18;
    rotationX    = std::clamp(rotationX, -rotRange, rotRange);

    _pTransform->SetRotationX(rotationX);
    _pTransform->RotateYAxis(input::MouseMgr()->GetPosX() * dt * _rotSpeed);
}

void InputControl::MoveFB(f32 speed)
{
    f32 dt = timer::TimerIns()->DeltaTime();
    _pTransform->Translate(_pTransform->GetForwardAxis(), dt * speed);
}

void InputControl::MoveLR(f32 speed)
{
    f32 dt = timer::TimerIns()->DeltaTime();
    _pTransform->Translate(_pTransform->GetRightAxis(), dt * speed);
}
}   // namespace component
