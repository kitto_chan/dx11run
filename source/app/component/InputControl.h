﻿//---------------------------------------------------------------------------
//! @file	InputControl.cpp
//!	@brief	プレイヤーの入力処理のコンポーネント
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"

namespace component {

class Transform;

class InputControl : public Component
{
public:
    InputControl();    //!< コンストラクタ
    ~InputControl();   //!< デストラクタ

public:
    //---------------------------------------------------------------------------
    //! 継承関数
    //---------------------------------------------------------------------------
    bool Init() override;          //!< 初期化
    void Update() override;        //!< 更新
    void RenderImgui() override;   //!< ImGui描画

private:
    //---------------------------------------------------------------------------
    //! private関数
    //---------------------------------------------------------------------------

    //! 移動
    void MoveControl();
    //! ビュー
    void LookControl();

    //! 前後移動(F-Forward B-Backward)
    void MoveFB(f32 speed);
    //! 左右移動(L-Left R-Right)
    void MoveLR(f32 speed);

    //---------------------------------------------------------------------------
    //! private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;   //!< Transformコンポーネントを取得

    f32 _moveSpeed = 5.f;    //!< 移動速度
    f32 _rotSpeed  = 1.5f;   //!< 視線回転の速度
};
}   // namespace component
