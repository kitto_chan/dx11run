﻿//---------------------------------------------------------------------------
//! @file	PlayerController.h
//!	@brief	プレイヤーの入力処理のコンポーネント
//---------------------------------------------------------------------------
#include "PlayerController.h"
#include "Component/Transform.h"
#include "entity/Entity.h"
namespace component {
namespace {
//! Transformコンポーネント
raw_ptr<Transform> _pTransform = nullptr;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
PlayerController::PlayerController()
{
    _name = _NAME;
}
//---------------------------------------------------------------------------
//! デスストラクタ
//---------------------------------------------------------------------------
PlayerController::~PlayerController()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool PlayerController::Init()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();

    //==========================================================
    // プレイヤーの衝突判定用のキャラクターを作成
    //==========================================================
    {
        _pPhyChara = physics::createCharacter(_pTransform->GetLocalToWorldMatrix(),
                                              _capsuleRadius,
                                              _capsuleHeight);
        if(!_pPhyChara) {
            ASSERT_MESSAGE(false, "物理キャラ初期化失敗");
            return false;
        }
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void PlayerController::Update()
{
    //==========================================================
    // プレイヤー操作
    //==========================================================
    Movement();   // 移動
    Jump();       // ジャンプ

    // 物理のポジションとコンポーネントの位置を統一する
    float3 phyTrans = math::makeTranslation(_pPhyChara->worldMatrix());
    _pTransform->SetPosition(phyTrans);
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void PlayerController::RenderImgui()
{
    ImGui::DragFloat("MoveSpeed", &_moveSpeed);
    ImGui::DragFloat("RotateSpeed", &_rotSpeed);
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void PlayerController::Finalize()
{
    _pPhyChara.reset();
}

//---------------------------------------------------------------------------
//! プレイヤーの移動処理
//---------------------------------------------------------------------------
void PlayerController::Movement()
{
    f32    dt        = timer::TimerIns()->DeltaTime();                     // デルタタイム
    f32    magnitude = dt * _moveSpeed;                                    // 移動距離
    float3 unitDir   = hlslpp::normalize(_pTransform->GetForwardAxis());   // ユニット
    float3 dir       = magnitude * unitDir;                                // 移動距離

//----------
// 8方向移動
#if 1
    f32 x = 0.0f;
    f32 z = 0.0f;
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::W)) {
        z = 1.0f;
    }
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::S)) {
        z = -1.0f;
    }
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::A)) {
        x = 1.0f;
    }
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::D)) {
        x = -1.0f;
    }

    // 移動
    if(z != 0.0f || x != 0.0f) {
        // 回転角度計算
        f32 angle = atan2f(x, z);
        _pTransform->RotateAxis(float3(0.0f, 1.0f, 0.0f), angle);
        _pPhyChara->walk(-dir);
    }
    else {
        // 停止
        _pPhyChara->walk(float3(0.f, 0.f, 0.f));
    }

//----------
// 回転移動
#else
    // 左転向
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::A)) {
        //MoveLR(_moveSpeed );
        _pTransform->RotateYAxis(DirectX::XMConvertToRadians(_rotSpeed));
    }
    // 右転向
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::D)) {
        //MoveLR(_moveSpeed * -1);
        _pTransform->RotateYAxis(DirectX::XMConvertToRadians(_rotSpeed * -1));
    }

    // 前進
    if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::W)) {
        _pPhyChara->walk(-dir);
    }
    // 後退
    else if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::S)) {
        _pPhyChara->walk(-dir);
    }
    // ストップ
    else {
        _pPhyChara->walk(float3(0.f, 0.f, 0.f));
    }
#endif
}
//---------------------------------------------------------------------------
//! プレイヤーのジャンプ処理
//---------------------------------------------------------------------------
void PlayerController::Jump()
{
    if(input::KeyboardMgr()->IsKeyPress(DirectX::Keyboard::Space)) {
        if(_pPhyChara->canJump()) {
            _pPhyChara->jump(_jumpForce);
        }
    }
}
}   // namespace component
