﻿//---------------------------------------------------------------------------
//!	@file	Collider.h
//!	@brief	すべてのコライダーの定義
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"

namespace component {
//===========================================================================
//! すべてのコライダーの基盤となるクラスです。
//===========================================================================
class Collider : public Component
{

public:
    Collider()          = default;   //!< コンストラクタ
    virtual ~Collider() = default;   //!< デストラクタ

    virtual uni_ptr<btCollisionShape> GetBtCollisionShape() { return std::move(_btShape); };

protected:
    std::unique_ptr<btCollisionShape> _btShape = nullptr;   //!< bulletシェープ
};

//===========================================================================
//! ボックスコライダーの基盤となるクラスです。
//===========================================================================
class BoxCollider : public Collider
{
public:
    BoxCollider(const float3& size)
    {
        _name        = "BoxCollider";
        _halfExtents = size / 2.0f;
        _btShape     = std::make_unique<btBoxShape>(btVector3(_halfExtents.x, _halfExtents.y, _halfExtents.z));
    }
    BoxCollider()  = delete;    //!< コンストラクタ
    ~BoxCollider() = default;   //!< デストラクタ

    virtual void RenderImgui() override;   //!< ImGui描画
private:
    float3 _halfExtents = 0.0f;   //!< ボックスの辺長
};
//===========================================================================
//!　球コライダーの基盤となるクラスです。
//===========================================================================
class SphereCollider : public Collider
{
public:
    SphereCollider() = delete;   //!< コンストラクタ
    SphereCollider(f32 radius)
    {
        _name    = "SphereCollider";
        _radius  = radius;
        _btShape = std::make_unique<btSphereShape>(radius);
    }
    ~SphereCollider() = default;   //!< デストラクタ

    virtual void RenderImgui() override;   //!< ImGui描画
private:
    f32 _radius = 0.0f;   //!< 半径
};
//===========================================================================
//! 円筒コライダーの基盤となるクラスです。
//===========================================================================
class CylinderCollider : public Collider
{
public:
    CylinderCollider() = delete;   //!< コンストラクタ
    CylinderCollider(f32 radius, f32 heightHalfExtent)
    {
        _name             = "CylinderCollider";
        _radius           = radius;
        _heightHalfExtent = heightHalfExtent;
        _btShape          = std::make_unique<btCylinderShape>(btVector3(radius, heightHalfExtent, radius));
    }
    ~CylinderCollider() = default;   //!< デストラクタ

    virtual void RenderImgui() override;   //!< ImGui描画
private:
    f32 _radius           = 0.0f;   //!< 半径
    f32 _heightHalfExtent = 0.0f;   //!< 高さの半分
};

}   // namespace component
