﻿//---------------------------------------------------------------------------
//!	@file	FloorAction.h
//!	@brief	床のアクションのクラス
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"

namespace component {
class Transform;
class RigidBody;
class RollingPinAction : public Component
{
    const std::string _NAME = "RollingPinAction";

public:
    RollingPinAction() = delete;   //!< コンストラクタ
    RollingPinAction(f32 fallTime);   //!< コンストラクタ
    ~RollingPinAction() = default;    //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 継承関数
    //---------------------------------------------------------------------------
    virtual bool Init();          //!< 初期化
    virtual void Update();        //!< 更新
    virtual void Render();        //!< 描画
    virtual void RenderImgui();   //!< ImGui描画
    virtual void Finalize();      //!< 解放

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------
    void SetDisableTime(f32 fallTime);

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    f32 _rollSpeed = 2.0f;

	f32 _counter = 0.0f;
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;
    raw_ptr<RigidBody> _pRigidBody = nullptr;
};
}   // namespace component
