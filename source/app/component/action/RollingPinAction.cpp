﻿//---------------------------------------------------------------------------
//!	@file	FloorAction.h
//!	@brief	床のアクションのクラス
//---------------------------------------------------------------------------
#include "RollingPinAction.h"
#include "entity/Entity.h"
#include "component/Transform.h"
#include "component/RigidBody.h"
namespace component {
namespace {
constexpr f32 DISABLE_DISTANCE = -10.0f;   //!< Y軸はこの距離以下なったらオブジェクト削除する
}
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
RollingPinAction::RollingPinAction(f32 fallTime)
{
    _name = _NAME;

    _counter = fallTime;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool RollingPinAction::Init()
{
    // 必要なコンポネントを揃えます
    _pTransform = _pOwner->GetComponent<component::Transform>();
    _pRigidBody = _pOwner->GetComponent<component::RigidBody>();

    ASSERT_MESSAGE(_pTransform, "Transform Componentが見つけません");
    ASSERT_MESSAGE(_pRigidBody, "RigidBody Componentが見つけません");

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void RollingPinAction::Update()
{
    f32 dt = timer::TimerIns()->DeltaTime();
    _pTransform->RotateYAxis(_rollSpeed * dt);

    _counter--;
    if(_counter <= 0.0f) {
        // rigibody解放
        if(_pRigidBody->IsEnable()) {
            _pRigidBody->Remove();
            _pRigidBody->Disable();
        }
        _pOwner->Release();
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void RollingPinAction::Render()
{
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void RollingPinAction::RenderImgui()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void RollingPinAction::Finalize()
{
}
void RollingPinAction::SetDisableTime(f32 fallTime)
{
    _counter = fallTime;
}
}   // namespace component
