﻿//---------------------------------------------------------------------------
//! @file	PlayerConller.cpp
//!	@brief	プレイヤーの入力処理のコンポーネント
//---------------------------------------------------------------------------
#pragma once
#include "physics/physics_character.h"
#include "Component.h"

namespace component {

class Transform;

class PlayerController : public Component
{
    const std::string _NAME = "PlayerController";

public:
    PlayerController();    //!< コンストラクタ
    ~PlayerController();   //!< デストラクタ

public:
    //===========================================================================
    //! 継承関数
    //===========================================================================
    bool Init() override;          //!< 初期化
    void Update() override;        //!< 更新
    void RenderImgui() override;   //!< ImGui描画
    void Finalize() override;      //!< 解放

private:
    //===========================================================================
    //! private関数
    //===========================================================================
    void Movement();   //!< プレイヤーの移動処理
    void Jump();       //!< プレイヤーのジャンプ処理
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------

    //! 物理のキャラコントローラ
    std::unique_ptr<physics::Character> _pPhyChara;

    f32 _capsuleHeight = 1.0f;   //!< カプセルrigibodyの高さ
    f32 _capsuleRadius = 0.5f;   //!< カプセルrigibodyの半径

    f32    _moveSpeed = 1.0f;                   //!< 移動速度
    f32    _rotSpeed  = 1.5f;                   //!< 回転の速度
    float3 _jumpForce = { 0.0f, 6.0f, 0.0f };   //!< ジャンプ
};
}   // namespace component
