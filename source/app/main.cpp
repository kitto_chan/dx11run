﻿//---------------------------------------------------------------------------
//!	@file	Main.cpp
//!	@brief	アプリケーションメイン
//---------------------------------------------------------------------------

/*! @mainpage ゲームエンジンについて

OCA大阪デザイン＆ITテクノロジー専門学校　チンセイケツ

// remark 1 モデル読み込むクラスバグあると思う

//---------------------------------------------------------------------------
// Phase 1  ~ 第1回制作　2021/06/14 まで
//---------------------------------------------------------------------------
チェック
[〇] E-C-S デザイン // TODO: 細かいところもっときれいにします
[〇] 画面でオブジェクトの位置、回転、スケールを扱う 
[〇] Transformコンポネント   // TODO: 回転はオイラー角　-> クォータニオン
[〇] スカイボックス
[〇] カメラ
[〇] フォグエフェクト
[〇] Imgui勉強

[△] 光
[△] ゲームエンジンのUI
[△] 先生のコード読めるように

// 間に合わない
[✕]　親子関係    -> Phase2でやる -> シラバス5回目教える
[✕] シリアライズ -> Phase2でやる

//---------------------------------------------------------------------------
// Phase 2  ~ 第2回制作
//---------------------------------------------------------------------------
[] 親子関係 (優先)
[] シリアライズ Library追加 (優先)

[] font icon library 追加
[] fmt library 追加

[] AssetManager　（画像、サウンド） DirectXTK
[] CameraManager 

[] Effect Class refactoring -> 今のデザインちょっとおかしいと思う
[] 2D UI                    -> シラバスによって 14回目やるので、教える内容次第に実装します

[] Model
[] Render Target
[] Screen Fade
[] 攻撃エフェクト

//---------------------------------------------------------------------------
// Phase 3  ~ 第3回制作
//---------------------------------------------------------------------------
// TODO 教える内容次第に実装してみます
[] エフェクトビルボー      -> シラバス15回目やる
[] 影　                   -> シラバス24,25,27,28回目やる
[] オフスクリーンレンダリン -> シラバス20回目やる
[] ポストエフェクトフィルタ -> シラバス21回目やる

//---------------------------------------------------------------------------
// いつか
//---------------------------------------------------------------------------
[] keyword: motion bulr

[鬼] マルチプラットフォーム (dx12, openGL) たぶん無理
 */

#include "GameApp.h"
#include "manager/SceneManager.h"
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
    dx11::callback::onInitialize = &manager::onInitialize;   // callback : 初期化
    dx11::callback::onUpdate     = &manager::onUpdate;       // callback : 更新
    dx11::callback::onRender     = &manager::onRender;       // callback : 描画
    dx11::callback::onFinalize   = &manager::onFinalize;     // callback : 解放

    GameApp gameApp(hInstance);

    if(!gameApp.Init())
        return 0;

    return gameApp.Run();
}

// Debug用　window consoleを表示
int main()
{
    return WinMain(GetModuleHandle(nullptr), nullptr, GetCommandLineA(), SW_NORMAL);
}
