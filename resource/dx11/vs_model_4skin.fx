//----------------------------------------------------------------------------
//!	@file	vs_model_4skin.fx
//!	@brief	3Dモデル4weightスキニング
//----------------------------------------------------------------------------

// 定数バッファ
cbuffer WorldCB : register(b0)
{
	matrix	matWorld_;  // ワールド行列
};

cbuffer CameraCB : register(b1)
{
	matrix	matView_;   // ビュー行列
	matrix	matProj_;   // 投影行列
};

cbuffer ModelCB : register(b2)
{
	matrix	matJoints_[512];	// 関節行列
};

// 頂点シェーダー入力
struct VS_INPUT
{
	float4	position_ : POSITION;
	float4	color_    : COLOR;
	float2	uv_       : TEXCOORD;

	float3	normal_   : NORMAL;
	int4	skinIndex_  : BLENDINDICES;
	float4	skinWeight_ : BLENDWEIGHT;
};

// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4	position_ : SV_Position;
	float4	color_    : COLOR;
	float2	uv_		  : TEXCOORD0;
	float3	normal_   : NORMAL;
};

//----------------------------------------------------------------------------
// 頂点シェーダー
//----------------------------------------------------------------------------
VS_OUTPUT main(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	//-----------------------------------------------------------
	// スキニング計算
	//-----------------------------------------------------------
	float4	position = 0;
	{
		int4	indices = input.skinIndex_;
		float4	weights = input.skinWeight_;
		for(int i=0; i<4; ++i ) {
			if( weights.x == 0.0) break;
			position += mul(matJoints_[indices.x], input.position_) * weights.x;
			indices = indices.yzwx;
			weights = weights.yzwx;
		}
		position.w = 1.0;
	}

	// 座標変換
	position         = mul(matWorld_, position);
	position         = mul(matView_, position);
	output.position_ = mul(matProj_, position);

	output.color_  = input.color_;
	output.uv_     = input.uv_;
	output.normal_ = input.normal_;

	//-----------------------------------------------------------
	// [DEBUG] スキニングの関節番号を簡易色分け表示
	//-----------------------------------------------------------
	if(0)
	{
		output.color_ = 0.0;
		for( int i=0; i<4; ++i ) {
			int n = input.skinIndex_.x;
			output.color_ += float4( float((n     ) & 1) * 0.5 + float((n >> 3) & 3) * 0.25,
									 float((n >> 1) & 1) * 0.5 + float((n >> 3) & 3) * 0.25,
									 float((n >> 2) & 1) * 0.5 + float((n >> 3) & 3) * 0.25,
									 1.0) * input.skinWeight_.x;
			input.skinIndex_  = input.skinIndex_.yzwx;
			input.skinWeight_ = input.skinWeight_.yzwx;
		}
	}

	//-----------------------------------------------------------
	// [DEBUG] 法線を簡易色分け表示
	//-----------------------------------------------------------
	if(0)
	{
		output.color_.rgb = output.normal_;
	}

	return output;
}
