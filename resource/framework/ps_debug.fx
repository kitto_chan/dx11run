//----------------------------------------------------------------------------
//!	@file	ps_debug.fx
//!	@brief	テクスチャなしピクセルシェーダー
//----------------------------------------------------------------------------

// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4	position_ : SV_Position;
	float4	color_    : COLOR;
};

// ピクセルシェーダー出力
struct PS_OUTPUT
{
	float4	color0_ : SV_Target0;
};

//----------------------------------------------------------------------------
// ピクセルシェーダー
//----------------------------------------------------------------------------
PS_OUTPUT main(VS_OUTPUT input)
{
	PS_OUTPUT	output = (PS_OUTPUT)0;

	output.color0_ = input.color_;
	return output;
}
