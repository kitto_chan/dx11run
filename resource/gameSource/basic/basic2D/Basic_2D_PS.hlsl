//----------------------------------------------------------------------------
//!	@file	Basic_2D_PS.hlsl
//!	@brief	基本的の汎用2Dピクセルシェーダ
//----------------------------------------------------------------------------
#include "../Basic.hlsli"

float4 PS(VertexPosHTex pIn) : SV_Target
{
    return g_tex.Sample(g_sam, pIn.Tex);
}
