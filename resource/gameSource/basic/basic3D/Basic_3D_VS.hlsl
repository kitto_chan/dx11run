//----------------------------------------------------------------------------
//!	@file	Basic_3D_VS.hlsl
//!	@brief	基本的の汎用3Dピクセルシェーダ
//----------------------------------------------------------------------------
#include "../Basic.hlsli"

VertexPosHWNormalTex VS(VertexPosNormalTex vIn)
{
    VertexPosHWNormalTex vOut;
    
	float4 posW = mul(matWorld, float4(vIn.PosL, 1.0f));
	float3 normalW = mul((float3x3) matWorldInv, vIn.NormalL);
    
 //   // 反射
 //   [flatten]
 //   if (g_isReflection)
 //   {
	//	posW = mul(g_reflection, posW);
	//	normalW = mul((float3x3) g_reflection, normalW);
	//}
 //   // 影
 //   [flatten]
 //   if (g_isShadow)
 //   {
	//	posW = (g_isReflection ? mul(g_refShadow, posW) : mul(g_shadow, posW));
	//}

	vOut.PosH = mul(matWorld, float4(vIn.PosL, 1.0f));
	vOut.PosH = mul(matView, vOut.PosH);
	vOut.PosH = mul(matProj, vOut.PosH);

    vOut.PosW = posW.xyz;
    vOut.NormalW = normalW;
    vOut.Tex = vIn.Tex;
    return vOut;
}
