//----------------------------------------------------------------------------
//!	@file	BasicPosCol.hlsl
//!	@brief	PosとColor専用ピクセルシェーダ
//----------------------------------------------------------------------------

#include "BasicPosCol.hlsli"
float4 PS(VertexOut pIn) : SV_Target
{
	return pIn.color;
}
