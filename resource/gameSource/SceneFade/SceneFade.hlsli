//----------------------------------------------------------------------------
//!	@file	SceneFade.hlsli
//!	@brief	シーンフェードシェーダヘーダ
//----------------------------------------------------------------------------
Texture2D texture0 : register(t0);
SamplerState sampler0 : register(s0);

cbuffer WVP_CB : register(b0)
{
    matrix matWVP_;
}

cbuffer FadeCB : register(b1)
{
    float fadeAmount_; //!< 色 (0.0f - 1.0f)
    float3 pad_;
}

struct VertexPosTex
{
    float3 posL : POSITION;
    float2 tex : TEXCOORD;
};

struct VertexPosHTex
{
    float4 posH : SV_POSITION;
    float2 tex : TEXCOORD;
};


