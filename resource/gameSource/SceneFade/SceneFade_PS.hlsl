//----------------------------------------------------------------------------
//!	@file	SceneFade.hlsl
//!	@brief	シーンフェードピクセルシェーダ
//----------------------------------------------------------------------------
#include "SceneFade.hlsli"

float4 PS(VertexPosHTex pIn) : SV_Target
{
	float4 fadeAmount = float4(fadeAmount_, fadeAmount_, fadeAmount_, 1.0f);
	return texture0.Sample(sampler0, pIn.tex) * fadeAmount;
}
